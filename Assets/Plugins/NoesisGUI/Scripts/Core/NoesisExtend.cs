using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Reflection;

namespace Noesis
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Classes with this attribute will be registered into Noesis engine, so they can be serialized
    // and unserialized, used for binding, etc.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct)]
    public class Extended : System.Attribute
    {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Specifies the source XAML file of a UserControl
    ////////////////////////////////////////////////////////////////////////////////////////////////
    [System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct)]
    public class UserControlSource : System.Attribute
    {
        internal string source = "";

        public UserControlSource(string xamlSource)
        {
            this.source = xamlSource;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Manages Noesis Extensibility
    ////////////////////////////////////////////////////////////////////////////////////////////////
    internal partial class Extend
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static bool Initialized { get; internal set; }
        public static bool ClassesRegistered { get; private set; }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void RegisterCallbacks()
        {
            // register callbacks
            Noesis_RegisterReflectionCallbacks(

                DependencyPropertyChanged,

                OnPostInit,

                CommandCanExecute,
                CommandExecute,

                GetName,
                GetBaseType,
                GetPropertiesCount,
                GetPropertyIndex,
                GetPropertyType,
                GetPropertyInfo,

                GetPropertyValue_Bool,
                GetPropertyValue_Float,
                GetPropertyValue_Int,
                GetPropertyValue_UInt,
                GetPropertyValue_Short,
                GetPropertyValue_UShort,
                GetPropertyValue_String,
                GetPropertyValue_Color,
                GetPropertyValue_Point,
                GetPropertyValue_Rect,
                GetPropertyValue_Size,
                GetPropertyValue_Thickness,
                GetPropertyValue_CornerRadius,
                GetPropertyValue_BaseComponent,

                SetPropertyValue_Bool,
                SetPropertyValue_Float,
                SetPropertyValue_Int,
                SetPropertyValue_UInt,
                SetPropertyValue_Short,
                SetPropertyValue_UShort,
                SetPropertyValue_String,
                SetPropertyValue_Color,
                SetPropertyValue_Point,
                SetPropertyValue_Rect,
                SetPropertyValue_Size,
                SetPropertyValue_Thickness,
                SetPropertyValue_CornerRadius,
                SetPropertyValue_BaseComponent,

                CreateInstance,
                DeleteInstance,
                GrabInstance);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void UnregisterCallbacks()
        {
            // unregister callbacks
            Noesis_RegisterReflectionCallbacks(
                null,
                null,
                null, null,
                null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null, null, null, null, null, null, null, null, null, null, null, null,
                null, null, null);

            mTypes.Clear();
            mExtends.Clear();
            mExtendTypes.Clear();
            PropertyMetadata.ClearCallbacks();

            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void RegisterExtendClasses()
        {
            System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            foreach (var A in assemblies)
            {
                System.Type[] types = null;
                try
                {
                    types = A.GetTypes();
                }
                catch (System.Reflection.ReflectionTypeLoadException)
                {
                    // the assembly contains one or more types that cannot be loaded (a type in the
                    // assembly is dependent on a type in an assembly that has not been loaded)
                    UnityEngine.Debug.LogWarning(System.String.Format(
                        "Unable to load some types in assembly '{0}' " +
                        "while trying to register Noesis extended types",
                        A.FullName));
                }

                if (types != null)
                {
                    foreach (var type in types)
                    {
                        if (type == null)
                        {
                            // this type failed to load
                            continue;
                        }

                        TryRegisterExtendType(type);
                    }
                }
            }
            
            ClassesRegistered = true;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void UnregisterExtendClasses()
        {
            if (ClassesRegistered)
            {
                System.Reflection.Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
                foreach (var A in assemblies)
                {
                    System.Type[] types = null;
                    try
                    {
                        types = A.GetTypes();
                    }
                    catch (System.Reflection.ReflectionTypeLoadException)
                    {
                        // the assembly contains one or more types that cannot be loaded (a type in the
                        // assembly is dependent on a type in an assembly that has not been loaded)
                    }

                    if (types != null)
                    {
                        foreach (var type in types)
                        {
                            if (type == null)
                            {
                                // this type failed to load
                                continue;
                            }

                            if (IsExtendType(type))
                            {
                                UnregisterDependencyProperties(type);
                            }
                        }
                    }
                }
                
                ClassesRegistered = false;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static bool IsExtendType(System.Type type)
        {
            return mExtendTypes.Contains(type);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void TryRegisterExtendType(System.Type type)
        {
            bool isExtended = false;
            UserControlSource userControlAttr = null;

            if (type.IsSubclassOf(typeof(Noesis.BaseComponent)))
            {
                System.Attribute[] attrs = System.Attribute.GetCustomAttributes(type);
                foreach (System.Attribute attr in attrs)
                {
                    isExtended |= attr is Noesis.Extended;

                    if (attr is Noesis.UserControlSource)
                    {
                        userControlAttr = (Noesis.UserControlSource)attr;
                    }
                }
            }

            if (isExtended)
            {
                System.Reflection.MethodInfo extend = FindExtendMethod(type);
                if (extend != null)
                {
                    extend.Invoke(null, new object[] { type });
                    RegisterDependencyProperties(type);
                    OverrideUserControlSource(type, userControlAttr);

                    mExtendTypes.Add(type);
                }
                else
                {
                    Debug.LogWarning("Can't find Extend method to register class " + type.Name);
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static System.Reflection.MethodInfo FindExtendMethod(System.Type type)
        {
            System.Reflection.MethodInfo extend = null;
            System.Type baseType = type.BaseType;
            while (extend == null && baseType != null)
            {
                extend = baseType.GetMethod("Extend", BindingFlags.NonPublic | BindingFlags.Static);
                baseType = baseType.BaseType;
            }

            return extend.GetParameters().Length == 1 ? extend : null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void RegisterDependencyProperties(System.Type type)
        {
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.IsStatic && field.FieldType == typeof(DependencyProperty))
                {
                    if (field.IsInitOnly)
                    {
                        throw new Exception("DependencyProperty fields cannot be readonly");
                    }

                    // ensure that static constructor is executed, so dependency properties
                    // are registered in Noesis reflection
                    DependencyProperty.RegisterCalled = false;
                    RuntimeHelpers.RunClassConstructor(type.TypeHandle);
                    if (!DependencyProperty.RegisterCalled)
                    {
                        // force static ctor execution
                        type.TypeInitializer.Invoke(null, null);
                    }

                    break;
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void UnregisterDependencyProperties(System.Type type)
        {
            FieldInfo[] fields = type.GetFields();
            foreach (FieldInfo field in fields)
            {
                if (field.IsStatic && field.FieldType == typeof(DependencyProperty))
                {
                    // set to null to free memory
                    field.SetValue(null, null);
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void OverrideUserControlSource(System.Type type, UserControlSource attr)
        {
            if (attr != null && type.IsSubclassOf(typeof(UserControl)))
            {
                if (attr.source == string.Empty)
                {
                    Debug.LogWarning("UserControl.Source is empty for class " + type.Name);
                }
                else
                {
                    UserControl.SourceProperty.OverrideMetadata(type, new PropertyMetadata(attr.source));
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static readonly HashSet<Type> mExtendTypes = new HashSet<Type>();

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static IntPtr GetPtrForType(System.Type type)
        {
            IntPtr unityType = type.TypeHandle.Value;

            if (!mTypes.ContainsKey(unityType))
            {
                mTypes.Add(unityType, type);
            }

            return unityType;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static System.Type GetTypeFromPtr(IntPtr unityType)
        {
            return mTypes[unityType];
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        static System.Collections.Generic.Dictionary<IntPtr, System.Type> mTypes =
            new System.Collections.Generic.Dictionary<IntPtr, System.Type>();

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static IntPtr GetResourceKeyType(System.Type type)
        {
            return Noesis_GetResourceKeyType(GetPtrForType(type));
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_DependencyPropertyChanged(IntPtr unityType, IntPtr cPtr,
            IntPtr dependencyPropertyPtr);

        [MonoPInvokeCallback(typeof(Callback_DependencyPropertyChanged))]
        private static void DependencyPropertyChanged(IntPtr unityType, IntPtr cPtr,
            IntPtr dependencyPropertyPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            MethodInfo methodInfo = type.GetMethod("DependencyPropertyChanged", new Type[] { typeof(DependencyProperty) });
            if (methodInfo != null)
            {
                DependencyProperty dependencyProperty =
                    new DependencyProperty(dependencyPropertyPtr, false);
        
                object[] parametersArray = new object[] { dependencyProperty };
                methodInfo.Invoke(instance, parametersArray);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_OnPostInit(IntPtr unityType, IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_OnPostInit))]
        private static void OnPostInit(IntPtr unityType, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            MethodInfo methodInfo = type.GetMethod("OnPostInit", new Type[] { });
            if (methodInfo != null)
            {
                object[] parametersArray = new object[] { };
                methodInfo.Invoke(instance, parametersArray);
            }
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate bool Callback_CommandCanExecute(IntPtr unityType, IntPtr cPtr,
            IntPtr paramPtr);

        [MonoPInvokeCallback(typeof(Callback_CommandCanExecute))]
        private static bool CommandCanExecute(IntPtr unityType, IntPtr cPtr,
            IntPtr paramPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            MethodInfo methodInfo = type.GetMethod("CanExecuteCommand");
            if (methodInfo != null)
            {
                BaseComponent param = new BaseComponent(paramPtr, false);
        
                object[] parametersArray = new object[] { param };
                return (bool)methodInfo.Invoke(instance, parametersArray);
            }
            
            return false;
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_CommandExecute(IntPtr unityType, IntPtr cPtr,
            IntPtr paramPtr);

        [MonoPInvokeCallback(typeof(Callback_CommandExecute))]
        private static void CommandExecute(IntPtr unityType, IntPtr cPtr,
            IntPtr paramPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
                
            MethodInfo methodInfo = type.GetMethod("ExecuteCommand");
            if (methodInfo != null)
            {
                BaseComponent param = new BaseComponent(paramPtr, false);
        
                object[] parametersArray = new object[] { param };
                methodInfo.Invoke(instance, parametersArray);
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate IntPtr Callback_GetName(IntPtr unityType);

        [MonoPInvokeCallback(typeof(Callback_GetName))]
        private static IntPtr GetName(IntPtr unityType)
        {
            Type type = GetTypeFromPtr(unityType);
            string fullName;
            if (type.Namespace != null && type.Namespace.Length > 0)
            {
                fullName = type.Namespace + "." + type.Name;
            }
            else
            {
                fullName = type.Name;
            }
            IntPtr baseNamePtr = Marshal.StringToHGlobalAnsi(fullName);
            return baseNamePtr;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate IntPtr Callback_GetBaseType(IntPtr unityType);

        [MonoPInvokeCallback(typeof(Callback_GetBaseType))]
        private static IntPtr GetBaseType(IntPtr unityType)
        {
            Type type = GetTypeFromPtr(unityType);
            return Extend.GetPtrForType(type.BaseType);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static PropertyInfo[] GetPublicProperties(Type type)
        {
            return type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate int Callback_GetPropertiesCount(IntPtr unityType);

        [MonoPInvokeCallback(typeof(Callback_GetPropertiesCount))]
        private static int GetPropertiesCount(IntPtr unityType)
        {
            Type type = GetTypeFromPtr(unityType);
            PropertyInfo[] properties = GetPublicProperties(type);
            return properties.Length;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate int Callback_GetPropertyIndex(IntPtr unityType, string propName);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyIndex))]
        private static int GetPropertyIndex(IntPtr unityType, string propName)
        {
            Type type = GetTypeFromPtr(unityType);
            PropertyInfo[] properties = GetPublicProperties(type);

            int index = 0;
            foreach (PropertyInfo propertyInfo in properties)
            {
                if (propertyInfo.Name == propName)
                {
                    return index;
                }

                ++index;
            }

            return -1;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private enum ReflectionTypes
        {
            Type_Invalid = 0,
            Type_Bool,
            Type_Float,
            Type_Int,
            Type_UInt,
            Type_Short,
            Type_UShort,
            Type_String,
            Type_Color,
            Type_Point,
            Type_Rect,
            Type_Size,
            Type_Thickness,
            Type_BaseComponent,
            Type_CornerRadius
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static int GetReflectionType(Type type)
        {
            int reflectionType = (int)ReflectionTypes.Type_Invalid;

            if (type == typeof(bool))
            {
                reflectionType = (int)ReflectionTypes.Type_Bool;
            }
            else if (type == typeof(float))
            {
                reflectionType = (int)ReflectionTypes.Type_Float;
            }
            else if (type == typeof(int))
            {
                reflectionType = (int)ReflectionTypes.Type_Int;
            }
            else if (type == typeof(uint))
            {
                reflectionType = (int)ReflectionTypes.Type_UInt;
            }
            else if (type == typeof(short))
            {
                reflectionType = (int)ReflectionTypes.Type_Short;
            }
            else if (type == typeof(ushort))
            {
                reflectionType = (int)ReflectionTypes.Type_UShort;
            }
            else if (type == typeof(string) || type.IsEnum)
            {
                reflectionType = (int)ReflectionTypes.Type_String;
            }
            else if (type == typeof(Color))
            {
                reflectionType = (int)ReflectionTypes.Type_Color;
            }
            else if (type == typeof(Point))
            {
                reflectionType = (int)ReflectionTypes.Type_Point;
            }
            else if (type == typeof(Rect))
            {
                reflectionType = (int)ReflectionTypes.Type_Rect;
            }
            else if (type == typeof(Size))
            {
                reflectionType = (int)ReflectionTypes.Type_Size;
            }
            else if (type == typeof(Thickness))
            {
                reflectionType = (int)ReflectionTypes.Type_Thickness;
            }
            else if (type == typeof(CornerRadius))
            {
                reflectionType = (int)ReflectionTypes.Type_CornerRadius;
            }
            else if (type == typeof(BaseComponent) || type.IsSubclassOf(typeof(BaseComponent)))
            {
                reflectionType = (int)ReflectionTypes.Type_BaseComponent;
            }

            return reflectionType;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate int Callback_GetPropertyType(IntPtr unityType, int propertyIndex);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyType))]
        private static int GetPropertyType(IntPtr unityType, int propertyIndex)
        {
            Type type = GetTypeFromPtr(unityType);

            PropertyInfo[] properties = GetPublicProperties(type);

            return GetReflectionType(properties[propertyIndex].PropertyType);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_GetPropertyInfo(IntPtr unityType, int propertyIndex,
            ref IntPtr name, [MarshalAs(UnmanagedType.U1)] ref bool read,
            [MarshalAs(UnmanagedType.U1)] ref bool write, ref int proptype);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyInfo))]
        private static void GetPropertyInfo(IntPtr unityType, int propertyIndex, ref IntPtr name,
            [MarshalAs(UnmanagedType.U1)] ref bool read, [MarshalAs(UnmanagedType.U1)] ref bool write,
            ref int proptype)
        {
            Type type = GetTypeFromPtr(unityType);
            
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];
            
            name = Marshal.StringToHGlobalAnsi(propertyInfo.Name);
            read = propertyInfo.CanRead;
            write = propertyInfo.CanWrite && propertyInfo.GetSetMethod(true).IsPublic;
            proptype = GetReflectionType(propertyInfo.PropertyType);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        [return: MarshalAs(UnmanagedType.U1)]
        private delegate bool Callback_GetPropertyValue_Bool(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Bool))]
        [return: MarshalAs(UnmanagedType.U1)]
        private static bool GetPropertyValue_Bool(IntPtr unityType, int propertyIndex,
            IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (bool)propertyInfo.GetValue(instance, null);
        }

        private delegate float Callback_GetPropertyValue_Float(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Float))]
        private static float GetPropertyValue_Float(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (float)propertyInfo.GetValue(instance, null);
        }

        private delegate int Callback_GetPropertyValue_Int(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Int))]
        private static int GetPropertyValue_Int(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (int)propertyInfo.GetValue(instance, null);
        }

        private delegate uint Callback_GetPropertyValue_UInt(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_UInt))]
        private static uint GetPropertyValue_UInt(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (uint)propertyInfo.GetValue(instance, null);
        }

        private delegate short Callback_GetPropertyValue_Short(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Short))]
        private static short GetPropertyValue_Short(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (short)propertyInfo.GetValue(instance, null);
        }

        private delegate ushort Callback_GetPropertyValue_UShort(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_UShort))]
        private static ushort GetPropertyValue_UShort(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            return (ushort)propertyInfo.GetValue(instance, null);
        }

        private delegate IntPtr Callback_GetPropertyValue_String(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_String))]
        private static IntPtr GetPropertyValue_String(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            object val = propertyInfo.GetValue(instance, null);
            string str = propertyInfo.PropertyType.IsEnum ? val.ToString() : (string)val;
            return Marshal.StringToHGlobalAnsi(str);
        }

        private delegate IntPtr Callback_GetPropertyValue_Color(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Color))]
        private static IntPtr GetPropertyValue_Color(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Color color = (Color)propertyInfo.GetValue(instance, null);
            return Color.getCPtr(color).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_Point(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Point))]
        private static IntPtr GetPropertyValue_Point(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Point point = (Point)propertyInfo.GetValue(instance, null);
            return Point.getCPtr(point).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_Rect(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Rect))]
        private static IntPtr GetPropertyValue_Rect(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Rect rect = (Rect)propertyInfo.GetValue(instance, null);
            return Rect.getCPtr(rect).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_Size(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Size))]
        private static IntPtr GetPropertyValue_Size(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Size size = (Size)propertyInfo.GetValue(instance, null);
            return Size.getCPtr(size).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_Thickness(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_Thickness))]
        private static IntPtr GetPropertyValue_Thickness(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Thickness thickness = (Thickness)propertyInfo.GetValue(instance, null);
            return Thickness.getCPtr(thickness).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_CornerRadius(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_CornerRadius))]
        private static IntPtr GetPropertyValue_CornerRadius(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            CornerRadius thickness = (CornerRadius)propertyInfo.GetValue(instance, null);
            return CornerRadius.getCPtr(thickness).Handle;
        }

        private delegate IntPtr Callback_GetPropertyValue_BaseComponent(IntPtr unityType, int propertyIndex,
            IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_GetPropertyValue_BaseComponent))]
        private static IntPtr GetPropertyValue_BaseComponent(IntPtr unityType, int propertyIndex, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            BaseComponent baseComponent = (BaseComponent)propertyInfo.GetValue(instance, null);
            return BaseComponent.getCPtr(baseComponent).Handle;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_SetPropertyValue_Bool(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, [MarshalAs(UnmanagedType.U1)] bool val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Bool))]
        private static void SetPropertyValue_Bool(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, [MarshalAs(UnmanagedType.U1)] bool val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        private delegate void Callback_SetPropertyValue_Float(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, float val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Float))]
        private static void SetPropertyValue_Float(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, float val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        private delegate void Callback_SetPropertyValue_Int(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, int val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Int))]
        private static void SetPropertyValue_Int(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, int val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        private delegate void Callback_SetPropertyValue_UInt(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, uint val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_UInt))]
        private static void SetPropertyValue_UInt(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, uint val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        private delegate void Callback_SetPropertyValue_Short(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, short val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Short))]
        private static void SetPropertyValue_Short(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, short val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        private delegate void Callback_SetPropertyValue_UShort(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, ushort val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_UShort))]
        private static void SetPropertyValue_UShort(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, ushort val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            propertyInfo.SetValue(instance, val, null);
        }

        public static object ParseEnum(Type enumType, string val, string owner, string prop)
        {
            if (!val.Contains(","))
            {
                try
                {
                    object retVal = Enum.Parse(enumType, val);
                    if (Enum.IsDefined(enumType, retVal))
                    {
                        return retVal;
                    }
                }
                catch (ArgumentException)
                {
                }
            }

            throw new Exception(String.Format("Invalid value '{0}' for property {1}.{2}", val, owner, prop));
        }

        private delegate void Callback_SetPropertyValue_String(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_String))]
        private static void SetPropertyValue_String(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            string valStr = Marshal.PtrToStringAnsi(val);
            object valObj = !propertyInfo.PropertyType.IsEnum ? valStr :
                ParseEnum(propertyInfo.PropertyType, valStr, type.Name, propertyInfo.Name);
            propertyInfo.SetValue(instance, valObj, null);
        }

        private delegate void Callback_SetPropertyValue_Color(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Color))]
        private static void SetPropertyValue_Color(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Color color = new Color(val, false);
            propertyInfo.SetValue(instance, color, null);
        }

        private delegate void Callback_SetPropertyValue_Point(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Point))]
        private static void SetPropertyValue_Point(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Point point = new Point(val, false);
            propertyInfo.SetValue(instance, point, null);
        }

        private delegate void Callback_SetPropertyValue_Rect(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Rect))]
        private static void SetPropertyValue_Rect(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Rect rect = new Rect(val, false);
            propertyInfo.SetValue(instance, rect, null);
        }

        private delegate void Callback_SetPropertyValue_Size(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Size))]
        private static void SetPropertyValue_Size(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Size size = new Size(val, false);
            propertyInfo.SetValue(instance, size, null);
        }

        private delegate void Callback_SetPropertyValue_Thickness(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_Thickness))]
        private static void SetPropertyValue_Thickness(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            Thickness thickness = new Thickness(val, false);
            propertyInfo.SetValue(instance, thickness, null);
        }

        private delegate void Callback_SetPropertyValue_CornerRadius(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_CornerRadius))]
        private static void SetPropertyValue_CornerRadius(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);

            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            CornerRadius corners = new CornerRadius(val, false);
            propertyInfo.SetValue(instance, corners, null);
        }

        private delegate void Callback_SetPropertyValue_BaseComponent(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val);

        [MonoPInvokeCallback(typeof(Callback_SetPropertyValue_BaseComponent))]
        private static void SetPropertyValue_BaseComponent(IntPtr unityType, int propertyIndex,
            IntPtr cPtr, IntPtr val)
        {
            Type type = GetTypeFromPtr(unityType);
            object instance = GetInstance(cPtr);
    
            PropertyInfo[] properties = GetPublicProperties(type);
            PropertyInfo propertyInfo = properties[propertyIndex];

            BaseComponent baseComponent = (BaseComponent)BaseComponent.AsType(
                propertyInfo.PropertyType, val, null);

            propertyInfo.SetValue(instance, baseComponent, null);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        [ThreadStatic]
        private static IntPtr _cPtr = IntPtr.Zero;
        [ThreadStatic]
        private static Type _extendType = null;

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static bool NeedsCreateCPtr(Type extendType)
        {
            return _cPtr == IntPtr.Zero || _extendType != extendType;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static IntPtr GetCPtr(BaseComponent instance, Type extendType)
        {
            if (_cPtr == IntPtr.Zero)
            {
                Debug.LogWarning("cPtr is null");
            }

            if (_extendType != extendType)
            {
                Debug.LogWarning("Invalid extend type");
            }

            // only one instance can use this C Ptr
            IntPtr cPtr = _cPtr;
            _cPtr = IntPtr.Zero;
            _extendType = null;

            // This function is called when a Extend proxy is created from C++
            // We add the Extend proxy to our table of extend instances
            AddExtendInfo(cPtr, instance);

            return cPtr;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static IntPtr New(System.Type type, BaseComponent instance)
        {
            IntPtr unityType = Extend.GetPtrForType(type);
            IntPtr cPtr = Noesis_InstantiateExtend(unityType);

            if (cPtr == IntPtr.Zero)
            {
                throw new System.Exception(String.Format("Unable to create an instance of '{0}'",
                    type.Name));
            }

            // This function is called when a Extend proxy is created from C#
            // We add the Extend proxy to our table of extend instances
            AddExtendInfo(cPtr, instance);

            return cPtr;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_CreateInstance(IntPtr unityType, IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_CreateInstance))]
        private static void CreateInstance(IntPtr unityType, IntPtr cPtr)
        {
            Type type = GetTypeFromPtr(unityType);

            _cPtr = cPtr;
            _extendType = type;
            Activator.CreateInstance(type, new object[] { });
            if (_cPtr != IntPtr.Zero)
            {
                Debug.LogWarning("cPtr was not used by any instance");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_DeleteInstance(IntPtr cPtr);

        [MonoPInvokeCallback(typeof(Callback_DeleteInstance))]
        private static void DeleteInstance(IntPtr cPtr)
        {
            RemoveExtendInfo(cPtr);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private delegate void Callback_GrabInstance(IntPtr cPtr, bool grab);

        [MonoPInvokeCallback(typeof(Callback_GrabInstance))]
        private static void GrabInstance(IntPtr cPtr, bool grab)
        {
            ExtendInfo extend = GetExtendInfo(cPtr);
            if (extend != null)
            {
                extend.instance = grab ? extend.weak.Target : null;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static object GetInstance(IntPtr cPtr)
        {
            ExtendInfo extend = GetExtendInfo(cPtr);
            return extend.weak.Target;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static ExtendInfo GetExtendInfo(IntPtr cPtr)
        {
            ExtendInfo extend = null;
            if (!mExtends.TryGetValue(cPtr, out extend))
            {
                throw new InvalidOperationException("Extend already removed");
            }
            else if (!extend.weak.IsAlive)
            {
                throw new InvalidOperationException("Extend already destroyed");
            }

            return extend;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static object TryGetInstance(IntPtr cPtr)
        {
            ExtendInfo extend = TryGetExtendInfo(cPtr);
            return extend != null ? extend.weak.Target : null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static ExtendInfo TryGetExtendInfo(IntPtr cPtr)
        {
            ExtendInfo extend = null;
            if (mExtends.TryGetValue(cPtr, out extend))
            {
                return extend;
            }

            return null;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void AddExtendInfo(IntPtr cPtr, BaseComponent instance)
        {
            ExtendInfo extend = new ExtendInfo();
            extend.instance = null;
            extend.weak = new WeakReference(instance);
            mExtends.Add(cPtr, extend);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static void RemoveExtendInfo(IntPtr cPtr)
        {
            mExtends.Remove(cPtr);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private class ExtendInfo
        {
            public object instance;
            public WeakReference weak;
        }

        static System.Collections.Generic.Dictionary<IntPtr, ExtendInfo> mExtends =
            new System.Collections.Generic.Dictionary<IntPtr, ExtendInfo>();

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void PropertyChanged(System.Type type, IntPtr cPtr, string propertyName)
        {
            IntPtr unityType = Extend.GetPtrForType(type);
            IntPtr propertyNamePtr = Marshal.StringToHGlobalAnsi(propertyName);
            Noesis_LaunchChangedEvent(unityType, cPtr, propertyNamePtr);
        }
    }
}
