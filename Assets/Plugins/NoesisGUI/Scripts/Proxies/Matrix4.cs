/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class Matrix4 : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Matrix4(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(Matrix4 obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~Matrix4() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          if (Noesis.Extend.Initialized) { NoesisGUI_PINVOKE.delete_Matrix4(swigCPtr);}
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public Vector4 this[uint i] {
    set {
      if (i >= 4) { throw new System.IndexOutOfRangeException(); }
      IndexSet(i, value);
    }
    get {
      if (i >= 4) { throw new System.IndexOutOfRangeException(); }
      return IndexGet(i);
    }
  }

  public static Matrix4 operator*(Matrix4 m, float f) {
    return new Matrix4(m[0] * f, m[1] * f, m[2] * f, m[3] * f);
  }

  public static Matrix4 operator*(float f, Matrix4 m) {
    return m * f;
  }

  public static Matrix4 operator/(Matrix4 m, float f) {
    if (f == 0.0f) { throw new System.DivideByZeroException(); }
    return new Matrix4(m[0] / f, m[1] / f, m[2] / f, m[3] / f);
  }

  public static Vector4 operator*(Vector4 v, Matrix4 m) {
    return new Vector4(
      v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + v[3] * m[3][0],
      v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + v[3] * m[3][1],
      v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + v[3] * m[3][2],
      v[0] * m[0][3] + v[1] * m[1][3] + v[2] * m[2][3] + v[3] * m[3][3]);
  }

  public static Matrix4 operator*(Matrix4 m0, Matrix4 m1) {
    return new Matrix4(
      new Vector4(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0] + m0[0][2] * m1[2][0] + m0[0][3] * m1[3][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1] + m0[0][2] * m1[2][1] + m0[0][3] * m1[3][1],
        m0[0][0] * m1[0][2] + m0[0][1] * m1[1][2] + m0[0][2] * m1[2][2] + m0[0][3] * m1[3][2],
        m0[0][0] * m1[0][3] + m0[0][1] * m1[1][3] + m0[0][2] * m1[2][3] + m0[0][3] * m1[3][3]),
      new Vector4(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0] + m0[1][2] * m1[2][0] + m0[1][3] * m1[3][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1] + m0[1][2] * m1[2][1] + m0[1][3] * m1[3][1],
        m0[1][0] * m1[0][2] + m0[1][1] * m1[1][2] + m0[1][2] * m1[2][2] + m0[1][3] * m1[3][2],
        m0[1][0] * m1[0][3] + m0[1][1] * m1[1][3] + m0[1][2] * m1[2][3] + m0[1][3] * m1[3][3]),
      new Vector4(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m0[2][2] * m1[2][0] + m0[2][3] * m1[3][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m0[2][2] * m1[2][1] + m0[2][3] * m1[3][1],
        m0[2][0] * m1[0][2] + m0[2][1] * m1[1][2] + m0[2][2] * m1[2][2] + m0[2][3] * m1[3][2],
        m0[2][0] * m1[0][3] + m0[2][1] * m1[1][3] + m0[2][2] * m1[2][3] + m0[2][3] * m1[3][3]),
      new Vector4(
        m0[3][0] * m1[0][0] + m0[3][1] * m1[1][0] + m0[3][2] * m1[2][0] + m0[3][3] * m1[3][0],
        m0[3][0] * m1[0][1] + m0[3][1] * m1[1][1] + m0[3][2] * m1[2][1] + m0[3][3] * m1[3][1],
        m0[3][0] * m1[0][2] + m0[3][1] * m1[1][2] + m0[3][2] * m1[2][2] + m0[3][3] * m1[3][2],
        m0[3][0] * m1[0][3] + m0[3][1] * m1[1][3] + m0[3][2] * m1[2][3] + m0[3][3] * m1[3][3])
    );
  }

  public static Matrix4 operator*(Transform3 m0, Matrix4 m1) {
    return new Matrix4(
      new Vector4(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0] + m0[0][2] * m1[2][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1] + m0[0][2] * m1[2][1],
        m0[0][0] * m1[0][2] + m0[0][1] * m1[1][2] + m0[0][2] * m1[2][2],
        m0[0][0] * m1[0][3] + m0[0][1] * m1[1][3] + m0[0][2] * m1[2][3]),
      new Vector4(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0] + m0[1][2] * m1[2][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1] + m0[1][2] * m1[2][1],
        m0[1][0] * m1[0][2] + m0[1][1] * m1[1][2] + m0[1][2] * m1[2][2],
        m0[1][0] * m1[0][3] + m0[1][1] * m1[1][3] + m0[1][2] * m1[2][3]),
      new Vector4(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m0[2][2] * m1[2][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m0[2][2] * m1[2][1],
        m0[2][0] * m1[0][2] + m0[2][1] * m1[1][2] + m0[2][2] * m1[2][2],
        m0[2][0] * m1[0][3] + m0[2][1] * m1[1][3] + m0[2][2] * m1[2][3]),
      new Vector4(
        m0[3][0] * m1[0][0] + m0[3][1] * m1[1][0] + m0[3][2] * m1[2][0] + m1[3][0],
        m0[3][0] * m1[0][1] + m0[3][1] * m1[1][1] + m0[3][2] * m1[2][1] + m1[3][1],
        m0[3][0] * m1[0][2] + m0[3][1] * m1[1][2] + m0[3][2] * m1[2][2] + m1[3][2],
        m0[3][0] * m1[0][3] + m0[3][1] * m1[1][3] + m0[3][2] * m1[2][3] + m1[3][3])
    );
  }

  public static Matrix4 operator*(Matrix4 m0, Transform3 m1) {
    return new Matrix4(
      new Vector4(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0] + m0[0][2] * m1[2][0] + m0[0][3] * m1[3][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1] + m0[0][2] * m1[2][1] + m0[0][3] * m1[3][1],
        m0[0][0] * m1[0][2] + m0[0][1] * m1[1][2] + m0[0][2] * m1[2][2] + m0[0][3] * m1[3][2],
                                                                          m0[0][3]),
      new Vector4(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0] + m0[1][2] * m1[2][0] + m0[1][3] * m1[3][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1] + m0[1][2] * m1[2][1] + m0[1][3] * m1[3][1],
        m0[1][0] * m1[0][2] + m0[1][1] * m1[1][2] + m0[1][2] * m1[2][2] + m0[1][3] * m1[3][2],
                                                                          m0[1][3]),
      new Vector4(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m0[2][2] * m1[2][0] + m0[2][3] * m1[3][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m0[2][2] * m1[2][1] + m0[2][3] * m1[3][1],
        m0[2][0] * m1[0][2] + m0[2][1] * m1[1][2] + m0[2][2] * m1[2][2] + m0[2][3] * m1[3][2],
                                                                          m0[2][3]),
      new Vector4(
        m0[3][0] * m1[0][0] + m0[3][1] * m1[1][0] + m0[3][2] * m1[2][0] + m0[3][3] * m1[3][0],
        m0[3][0] * m1[0][1] + m0[3][1] * m1[1][1] + m0[3][2] * m1[2][1] + m0[3][3] * m1[3][1],
        m0[3][0] * m1[0][2] + m0[3][1] * m1[1][2] + m0[3][2] * m1[2][2] + m0[3][3] * m1[3][2],
                                                                          m0[3][3])
    );
  }

  public static bool operator==(Matrix4 m0, Matrix4 m1) {
    // If both are null, or both are same instance, return true
    if (System.Object.ReferenceEquals(m0, m1)) {
      return true;
    }

    // If one is null, but not both, return false
    if (((System.Object)m0 == null) || ((System.Object)m1 == null)) {
      return false;
    }

    // Return true if the fields match
    return m0[0] == m1[0] && m0[1] == m1[1] && m0[2] == m1[2] && m0[3] == m1[3];
  }

  public static bool operator!=(Matrix4 m0, Matrix4 m1) {
    return !(m0 == m1);
  }

  public override bool Equals(System.Object obj) {
    // If parameter is null return false
    if (obj == null) {
      return false;
    }

    // If parameter cannot be cast return false
    Matrix4 m = obj as Matrix4;
    if ((System.Object)m == null) {
      return false;
    }

    // Return true if the fields match
    return this[0] == m[0] && this[1] == m[1] && this[2] == m[2] && this[3] == m[3];
  }

  public bool Equals(Matrix4 m) {
    // If parameter is null return false
    if ((System.Object)m == null) {
        return false;
    }

    // Return true if the fields match
    return this[0] == m[0] && this[1] == m[1] && this[2] == m[2] && this[3] == m[3];
  }

  public override int GetHashCode() {
    return ((this[0].GetHashCode() ^ this[1].GetHashCode()) ^ this[2].GetHashCode()) ^ this[3].GetHashCode();
  }

  public Matrix4() : this(NoesisGUI_PINVOKE.new_Matrix4__SWIG_0(), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix4(Matrix4 m) : this(NoesisGUI_PINVOKE.new_Matrix4__SWIG_1(Matrix4.getCPtr(m)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20, float m21, float m22, float m23, float m30, float m31, float m32, float m33) : this(NoesisGUI_PINVOKE.new_Matrix4__SWIG_2(m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix4(Vector4 v0, Vector4 v1, Vector4 v2, Vector4 v3) : this(NoesisGUI_PINVOKE.new_Matrix4__SWIG_3(Vector4.getCPtr(v0), Vector4.getCPtr(v1), Vector4.getCPtr(v2), Vector4.getCPtr(v3)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix4(Transform3 m) : this(NoesisGUI_PINVOKE.new_Matrix4__SWIG_4(Transform3.getCPtr(m)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static Matrix4 Identity() {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Identity(), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Scale(float scaleX, float scaleY, float scaleZ) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Scale(scaleX, scaleY, scaleZ), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 RotX(float radians) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_RotX(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 RotY(float radians) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_RotY(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 RotZ(float radians) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_RotZ(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Ortho(float width, float height, float zNear, float zFar) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Ortho__SWIG_0(width, height, zNear, zFar), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Ortho(float left, float right, float bottom, float top, float zNear, float zFar) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Ortho__SWIG_1(left, right, bottom, top, zNear, zFar), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Perspective(float width, float height, float zNear, float zFar) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Perspective__SWIG_0(width, height, zNear, zFar), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 PerspectiveFov(float fovY, float aspect, float zNear, float zFar) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_PerspectiveFov(fovY, aspect, zNear, zFar), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Perspective(float left, float right, float bottom, float top, float zNear, float zFar) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Perspective__SWIG_1(left, right, bottom, top, zNear, zFar), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Viewport(float width, float height) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Viewport(width, height), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Transpose(Matrix4 m) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Transpose(Matrix4.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static bool IsAffine(Matrix4 m) {
    bool ret = NoesisGUI_PINVOKE.Matrix4_IsAffine(Matrix4.getCPtr(m));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static float Determinant(Matrix4 m) {
    float ret = NoesisGUI_PINVOKE.Matrix4_Determinant(Matrix4.getCPtr(m));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Inverse(Matrix4 m) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Inverse__SWIG_0(Matrix4.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix4 Inverse(Matrix4 m, float determinant) {
    Matrix4 ret = new Matrix4(NoesisGUI_PINVOKE.Matrix4_Inverse__SWIG_1(Matrix4.getCPtr(m), determinant), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  private Vector4 IndexGet(uint i) {
    Vector4 ret = new Vector4(NoesisGUI_PINVOKE.Matrix4_IndexGet(swigCPtr, i), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  private void IndexSet(uint i, Vector4 v) {
    NoesisGUI_PINVOKE.Matrix4_IndexSet(swigCPtr, i, Vector4.getCPtr(v));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

}

}

