/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class EasingFunctionBase : Freezable {

  internal EasingFunctionBase(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.EasingFunctionBase_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(EasingFunctionBase obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  protected EasingFunctionBase() {
  }

  public EasingMode GetEasingMode() {
    EasingMode ret = (EasingMode)NoesisGUI_PINVOKE.EasingFunctionBase_GetEasingMode(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetEasingMode(EasingMode mode) {
    NoesisGUI_PINVOKE.EasingFunctionBase_SetEasingMode(swigCPtr, (int)mode);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public float Ease(float normalizedTime) {
    float ret = NoesisGUI_PINVOKE.EasingFunctionBase_Ease(swigCPtr, normalizedTime);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static DependencyProperty EasingModeProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.EasingFunctionBase_EasingModeProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_EasingFunctionBase(valPtr);
  }

}

}

