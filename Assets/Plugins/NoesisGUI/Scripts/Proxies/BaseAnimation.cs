/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class BaseAnimation : AnimationTimeline {

  internal BaseAnimation(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.BaseAnimation_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(BaseAnimation obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  protected BaseAnimation() {
  }

  public EasingFunctionBase GetEasingFunction() {
    IntPtr cPtr = NoesisGUI_PINVOKE.BaseAnimation_GetEasingFunction(swigCPtr);
    EasingFunctionBase ret = (cPtr == IntPtr.Zero) ? null : new EasingFunctionBase(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetEasingFunction(EasingFunctionBase function) {
    NoesisGUI_PINVOKE.BaseAnimation_SetEasingFunction(swigCPtr, EasingFunctionBase.getCPtr(function));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty EasingFunctionProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseAnimation_EasingFunctionProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_BaseAnimation(valPtr);
  }

}

}

