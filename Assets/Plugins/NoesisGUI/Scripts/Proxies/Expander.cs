/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class Expander : HeaderedContentControl {

  internal Expander(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.Expander_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(Expander obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }


  public delegate void CollapsedDelegate(BaseComponent sender, RoutedEventArgs e);
  public event CollapsedDelegate Collapsed {
    add {
      if (!_Collapsed.ContainsKey(swigCPtr.Handle)) {
        _Collapsed.Add(swigCPtr.Handle, null);

        NoesisGUI_PINVOKE.BindEvent_Expander_Collapsed(RaiseCollapsed, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif
      }

      _Collapsed[swigCPtr.Handle] += value;
    }
    remove {
      if (!_Collapsed.ContainsKey(swigCPtr.Handle)) {
        throw new System.Exception("Delegate not found");
      }

      _Collapsed[swigCPtr.Handle] -= value;

      if (_Collapsed[swigCPtr.Handle] == null) {
        NoesisGUI_PINVOKE.UnbindEvent_Expander_Collapsed(RaiseCollapsed, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif

        _Collapsed.Remove(swigCPtr.Handle);
      }
    }
  }

  internal delegate void DelegateRaiseCollapsed(IntPtr cPtr, IntPtr sender, IntPtr e);

  [MonoPInvokeCallback(typeof(DelegateRaiseCollapsed))]
  private static void RaiseCollapsed(IntPtr cPtr, IntPtr sender, IntPtr e) {
    if (!_Collapsed.ContainsKey(cPtr)) {
      throw new System.Exception("Delegate not found");
    }
    if (sender == System.IntPtr.Zero && e == System.IntPtr.Zero) {
      _Collapsed.Remove(cPtr);
      return;
    }
    if (_Collapsed[cPtr] != null) {
      _Collapsed[cPtr](new BaseComponent(sender, false), new RoutedEventArgs(e, false));
    }
  }

  static System.Collections.Generic.Dictionary<System.IntPtr, CollapsedDelegate> _Collapsed =
      new System.Collections.Generic.Dictionary<System.IntPtr, CollapsedDelegate>();


  public delegate void ExpandedDelegate(BaseComponent sender, RoutedEventArgs e);
  public event ExpandedDelegate Expanded {
    add {
      if (!_Expanded.ContainsKey(swigCPtr.Handle)) {
        _Expanded.Add(swigCPtr.Handle, null);

        NoesisGUI_PINVOKE.BindEvent_Expander_Expanded(RaiseExpanded, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif
      }

      _Expanded[swigCPtr.Handle] += value;
    }
    remove {
      if (!_Expanded.ContainsKey(swigCPtr.Handle)) {
        throw new System.Exception("Delegate not found");
      }

      _Expanded[swigCPtr.Handle] -= value;

      if (_Expanded[swigCPtr.Handle] == null) {
        NoesisGUI_PINVOKE.UnbindEvent_Expander_Expanded(RaiseExpanded, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif

        _Expanded.Remove(swigCPtr.Handle);
      }
    }
  }

  internal delegate void DelegateRaiseExpanded(IntPtr cPtr, IntPtr sender, IntPtr e);

  [MonoPInvokeCallback(typeof(DelegateRaiseExpanded))]
  private static void RaiseExpanded(IntPtr cPtr, IntPtr sender, IntPtr e) {
    if (!_Expanded.ContainsKey(cPtr)) {
      throw new System.Exception("Delegate not found");
    }
    if (sender == System.IntPtr.Zero && e == System.IntPtr.Zero) {
      _Expanded.Remove(cPtr);
      return;
    }
    if (_Expanded[cPtr] != null) {
      _Expanded[cPtr](new BaseComponent(sender, false), new RoutedEventArgs(e, false));
    }
  }

  static System.Collections.Generic.Dictionary<System.IntPtr, ExpandedDelegate> _Expanded =
      new System.Collections.Generic.Dictionary<System.IntPtr, ExpandedDelegate>();


  public Expander() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(Expander)) {
      return NoesisGUI_PINVOKE.new_Expander();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public ExpandDirection GetExpandDirection() {
    ExpandDirection ret = (ExpandDirection)NoesisGUI_PINVOKE.Expander_GetExpandDirection(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetExpandDirection(ExpandDirection direction) {
    NoesisGUI_PINVOKE.Expander_SetExpandDirection(swigCPtr, (int)direction);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetIsExpanded() {
    bool ret = NoesisGUI_PINVOKE.Expander_GetIsExpanded(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetIsExpanded(bool isExpanded) {
    NoesisGUI_PINVOKE.Expander_SetIsExpanded(swigCPtr, isExpanded);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty ExpandDirectionProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.Expander_ExpandDirectionProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsExpandedProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.Expander_IsExpandedProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr unityType = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_Expander(unityType);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_Expander(valPtr);
  }

}

}

