using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class UIPropertyMetadata
    {
        public UIPropertyMetadata(object defaultValue)
            : this(Create(defaultValue, null), true)
        {
        }

        public UIPropertyMetadata(object defaultValue, PropertyChangedCallback propertyChangedCallback)
            : this(Create(defaultValue, propertyChangedCallback), true)
        {
        }

        private static IntPtr Create(object defaultValue, PropertyChangedCallback propertyChangedCallback)
        {
            IntPtr propertyMetadataPtr = IntPtr.Zero;
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback =
                propertyChangedCallback == null ?
                    (DelegateInvokePropertyChangedCallback)null : InvokePropertyChangedCallback;

            if (defaultValue != null && defaultValue.GetType().IsEnum)
            {
                defaultValue = defaultValue.ToString();
            }

            if (defaultValue == null)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_BaseComponent(IntPtr.Zero,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is bool)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Bool((bool)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is float)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Float((float)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is int)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Int((int)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is uint)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_UInt((uint)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is short)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Short((short)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is ushort)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_UShort((ushort)defaultValue,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is string)
            {
                IntPtr strPtr = Marshal.StringToHGlobalAnsi((string)defaultValue);
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_String(strPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Color)
            {
                IntPtr defPtr = Color.getCPtr((Color)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Color(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Point)
            {
                IntPtr defPtr = Point.getCPtr((Point)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Point(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Rect)
            {
                IntPtr defPtr = Rect.getCPtr((Rect)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Rect(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Size)
            {
                IntPtr defPtr = Size.getCPtr((Size)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Size(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Thickness)
            {
                IntPtr defPtr = Thickness.getCPtr((Thickness)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_Thickness(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is CornerRadius)
            {
                IntPtr defPtr = CornerRadius.getCPtr((CornerRadius)defaultValue).Handle;
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_CornerRadius(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is Type)
            {
                IntPtr defPtr = Noesis.Extend.GetResourceKeyType(defaultValue as Type);
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_BaseComponent(defPtr,
                    invokePropertyChangedCallback);
            }
            else if (defaultValue is BaseComponent)
            {
                propertyMetadataPtr = Noesis_CreateUIPropertyMetadata_BaseComponent(
                    BaseComponent.getCPtr((BaseComponent)defaultValue).Handle,
                    invokePropertyChangedCallback);
            }

            if (propertyMetadataPtr == IntPtr.Zero)
            {
                throw new System.Exception("Default value type not supported");
            }

            // Register property changed callback
            if (propertyChangedCallback != null)
            {
                _PropertyChangedCallback.Add(propertyMetadataPtr, propertyChangedCallback);
            }

            return propertyMetadataPtr;
        }

    #if UNITY_EDITOR

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public new static void RegisterFunctions(Library lib)
        {
            // create UIPropertyMetadata 
            _CreateUIPropertyMetadata_Bool = lib.Find<CreateUIPropertyMetadataDelegate_Bool>(
                "Noesis_CreateUIPropertyMetadata_Bool");
            _CreateUIPropertyMetadata_Float = lib.Find<CreateUIPropertyMetadataDelegate_Float>(
                "Noesis_CreateUIPropertyMetadata_Float");
            _CreateUIPropertyMetadata_Int = lib.Find<CreateUIPropertyMetadataDelegate_Int>(
                "Noesis_CreateUIPropertyMetadata_Int");
            _CreateUIPropertyMetadata_UInt = lib.Find<CreateUIPropertyMetadataDelegate_UInt>(
                "Noesis_CreateUIPropertyMetadata_UInt");
            _CreateUIPropertyMetadata_Short = lib.Find<CreateUIPropertyMetadataDelegate_Short>(
                "Noesis_CreateUIPropertyMetadata_Short");
            _CreateUIPropertyMetadata_UShort = lib.Find<CreateUIPropertyMetadataDelegate_UShort>(
                "Noesis_CreateUIPropertyMetadata_UShort");
            _CreateUIPropertyMetadata_String = lib.Find<CreateUIPropertyMetadataDelegate_String>(
                "Noesis_CreateUIPropertyMetadata_String");
            _CreateUIPropertyMetadata_Color = lib.Find<CreateUIPropertyMetadataDelegate_Color>(
                "Noesis_CreateUIPropertyMetadata_Color");
            _CreateUIPropertyMetadata_Point = lib.Find<CreateUIPropertyMetadataDelegate_Point>(
                "Noesis_CreateUIPropertyMetadata_Point");
            _CreateUIPropertyMetadata_Rect = lib.Find<CreateUIPropertyMetadataDelegate_Rect>(
                "Noesis_CreateUIPropertyMetadata_Rect");
            _CreateUIPropertyMetadata_Size = lib.Find<CreateUIPropertyMetadataDelegate_Size>(
                "Noesis_CreateUIPropertyMetadata_Size");
            _CreateUIPropertyMetadata_Thickness = lib.Find<CreateUIPropertyMetadataDelegate_Thickness>(
                "Noesis_CreateUIPropertyMetadata_Thickness");
            _CreateUIPropertyMetadata_CornerRadius = lib.Find<CreateUIPropertyMetadataDelegate_CornerRadius>(
                "Noesis_CreateUIPropertyMetadata_CornerRadius");
            _CreateUIPropertyMetadata_BaseComponent = lib.Find<CreateUIPropertyMetadataDelegate_BaseComponent>(
                "Noesis_CreateUIPropertyMetadata_BaseComponent");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public new static void UnregisterFunctions()
        {
            // create UIPropertyMetadata 
            _CreateUIPropertyMetadata_Bool = null;
            _CreateUIPropertyMetadata_Float = null;
            _CreateUIPropertyMetadata_Int = null;
            _CreateUIPropertyMetadata_UInt = null;
            _CreateUIPropertyMetadata_Short = null;
            _CreateUIPropertyMetadata_UShort = null;
            _CreateUIPropertyMetadata_String = null;
            _CreateUIPropertyMetadata_Color = null;
            _CreateUIPropertyMetadata_Point = null;
            _CreateUIPropertyMetadata_Rect = null;
            _CreateUIPropertyMetadata_Size = null;
            _CreateUIPropertyMetadata_Thickness = null;
            _CreateUIPropertyMetadata_CornerRadius = null;
            _CreateUIPropertyMetadata_BaseComponent = null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Bool(bool defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Bool _CreateUIPropertyMetadata_Bool;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Bool([MarshalAs(UnmanagedType.U1)] bool defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Bool(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Float(float defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Float _CreateUIPropertyMetadata_Float;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Float(float defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Float(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Int(int defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Int _CreateUIPropertyMetadata_Int;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Int(int defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Int(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_UInt(uint defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_UInt _CreateUIPropertyMetadata_UInt;
        private static IntPtr Noesis_CreateUIPropertyMetadata_UInt(uint defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_UInt(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Short(short defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Short _CreateUIPropertyMetadata_Short;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Short(short defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Short(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_UShort(ushort defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_UShort _CreateUIPropertyMetadata_UShort;
        private static IntPtr Noesis_CreateUIPropertyMetadata_UShort(ushort defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_UShort(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_String(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_String _CreateUIPropertyMetadata_String;
        private static IntPtr Noesis_CreateUIPropertyMetadata_String(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_String(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Color(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Color _CreateUIPropertyMetadata_Color;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Color(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Color(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Point(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Point _CreateUIPropertyMetadata_Point;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Point(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Point(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Rect(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Rect _CreateUIPropertyMetadata_Rect;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Rect(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Rect(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Size(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Size _CreateUIPropertyMetadata_Size;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Size(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Size(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_Thickness(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_Thickness _CreateUIPropertyMetadata_Thickness;
        private static IntPtr Noesis_CreateUIPropertyMetadata_Thickness(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_Thickness(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_CornerRadius(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_CornerRadius _CreateUIPropertyMetadata_CornerRadius;
        private static IntPtr Noesis_CreateUIPropertyMetadata_CornerRadius(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_CornerRadius(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateUIPropertyMetadataDelegate_BaseComponent(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);
        static CreateUIPropertyMetadataDelegate_BaseComponent _CreateUIPropertyMetadata_BaseComponent;
        private static IntPtr Noesis_CreateUIPropertyMetadata_BaseComponent(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback)
        {
            IntPtr result = _CreateUIPropertyMetadata_BaseComponent(defaultValue, invokePropertyChangedCallback);
            Error.Check();
            return result;
        }

#else

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Bool")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Bool")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Bool([MarshalAs(UnmanagedType.U1)]bool defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Float")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Float")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Float(float defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Int")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Int")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Int(int defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_UInt")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_UInt")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_UInt(uint defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Short")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Short")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Short(short defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_UShort")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_UShort")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_UShort(ushort defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_String")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_String")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_String(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Color")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Color")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Color(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Point")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Point")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Point(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Rect")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Rect")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Rect(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Size")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Size")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Size(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_Thickness")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_Thickness")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_Thickness(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_CornerRadius")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_CornerRadius")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_CornerRadius(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateUIPropertyMetadata_BaseComponent")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateUIPropertyMetadata_BaseComponent")]
        #endif
        private static extern IntPtr Noesis_CreateUIPropertyMetadata_BaseComponent(IntPtr defaultValue,
            DelegateInvokePropertyChangedCallback invokePropertyChangedCallback);

    #endif

    }

}
