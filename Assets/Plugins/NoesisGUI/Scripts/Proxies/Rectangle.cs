/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class Rectangle : Shape {

  internal Rectangle(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.Rectangle_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(Rectangle obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public Rectangle() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(Rectangle)) {
      return NoesisGUI_PINVOKE.new_Rectangle();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public float GetRadiusX() {
    float ret = NoesisGUI_PINVOKE.Rectangle_GetRadiusX(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetRadiusX(float radius) {
    NoesisGUI_PINVOKE.Rectangle_SetRadiusX(swigCPtr, radius);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public float GetRadiusY() {
    float ret = NoesisGUI_PINVOKE.Rectangle_GetRadiusY(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetRadiusY(float radius) {
    NoesisGUI_PINVOKE.Rectangle_SetRadiusY(swigCPtr, radius);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty RadiusXProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.Rectangle_RadiusXProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty RadiusYProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.Rectangle_RadiusYProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr unityType = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_Rectangle(unityType);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_Rectangle(valPtr);
  }

}

}

