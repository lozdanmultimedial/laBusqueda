/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class ScrollViewer : ContentControl {

  internal ScrollViewer(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.ScrollViewer_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(ScrollViewer obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }


  public delegate void ScrollChangedDelegate(BaseComponent sender, ScrollChangedEventArgs e);
  public event ScrollChangedDelegate ScrollChanged {
    add {
      if (!_ScrollChanged.ContainsKey(swigCPtr.Handle)) {
        _ScrollChanged.Add(swigCPtr.Handle, null);

        NoesisGUI_PINVOKE.BindEvent_ScrollViewer_ScrollChanged(RaiseScrollChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif
      }

      _ScrollChanged[swigCPtr.Handle] += value;
    }
    remove {
      if (!_ScrollChanged.ContainsKey(swigCPtr.Handle)) {
        throw new System.Exception("Delegate not found");
      }

      _ScrollChanged[swigCPtr.Handle] -= value;

      if (_ScrollChanged[swigCPtr.Handle] == null) {
        NoesisGUI_PINVOKE.UnbindEvent_ScrollViewer_ScrollChanged(RaiseScrollChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif

        _ScrollChanged.Remove(swigCPtr.Handle);
      }
    }
  }

  internal delegate void DelegateRaiseScrollChanged(IntPtr cPtr, IntPtr sender, IntPtr e);

  [MonoPInvokeCallback(typeof(DelegateRaiseScrollChanged))]
  private static void RaiseScrollChanged(IntPtr cPtr, IntPtr sender, IntPtr e) {
    if (!_ScrollChanged.ContainsKey(cPtr)) {
      throw new System.Exception("Delegate not found");
    }
    if (sender == System.IntPtr.Zero && e == System.IntPtr.Zero) {
      _ScrollChanged.Remove(cPtr);
      return;
    }
    if (_ScrollChanged[cPtr] != null) {
      _ScrollChanged[cPtr](new BaseComponent(sender, false), new ScrollChangedEventArgs(e, false));
    }
  }

  static System.Collections.Generic.Dictionary<System.IntPtr, ScrollChangedDelegate> _ScrollChanged =
      new System.Collections.Generic.Dictionary<System.IntPtr, ScrollChangedDelegate>();


  public ScrollViewer() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(ScrollViewer)) {
      return NoesisGUI_PINVOKE.new_ScrollViewer();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public static ScrollBarVisibility GetHorizontalScrollBarVisibility(DependencyObject element) {
    ScrollBarVisibility ret = (ScrollBarVisibility)NoesisGUI_PINVOKE.ScrollViewer_GetHorizontalScrollBarVisibility(DependencyObject.getCPtr(element));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void SetHorizontalScrollBarVisibility(DependencyObject element, ScrollBarVisibility visibility) {
    NoesisGUI_PINVOKE.ScrollViewer_SetHorizontalScrollBarVisibility(DependencyObject.getCPtr(element), (int)visibility);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static ScrollBarVisibility GetVerticalScrollBarVisibility(DependencyObject element) {
    ScrollBarVisibility ret = (ScrollBarVisibility)NoesisGUI_PINVOKE.ScrollViewer_GetVerticalScrollBarVisibility(DependencyObject.getCPtr(element));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void SetVerticalScrollBarVisibility(DependencyObject element, ScrollBarVisibility visibility) {
    NoesisGUI_PINVOKE.ScrollViewer_SetVerticalScrollBarVisibility(DependencyObject.getCPtr(element), (int)visibility);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetCanContentScroll() {
    bool ret = NoesisGUI_PINVOKE.ScrollViewer_GetCanContentScroll(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetCanContentScroll(bool canScroll) {
    NoesisGUI_PINVOKE.ScrollViewer_SetCanContentScroll(swigCPtr, canScroll);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public PanningMode GetPanningMode() {
    PanningMode ret = (PanningMode)NoesisGUI_PINVOKE.ScrollViewer_GetPanningMode(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetPanningMode(PanningMode panningMode) {
    NoesisGUI_PINVOKE.ScrollViewer_SetPanningMode(swigCPtr, (int)panningMode);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public float GetPanningDeceleration() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetPanningDeceleration(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetPanningDeceleration(float deceleration) {
    NoesisGUI_PINVOKE.ScrollViewer_SetPanningDeceleration(swigCPtr, deceleration);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public float GetPanningRatio() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetPanningRatio(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetPanningRatio(float panningRatio) {
    NoesisGUI_PINVOKE.ScrollViewer_SetPanningRatio(swigCPtr, panningRatio);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Visibility GetComputedHorizontalScrollBarVisibility() {
    Visibility ret = (Visibility)NoesisGUI_PINVOKE.ScrollViewer_GetComputedHorizontalScrollBarVisibility(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public Visibility GetComputedVerticalScrollBarVisibility() {
    Visibility ret = (Visibility)NoesisGUI_PINVOKE.ScrollViewer_GetComputedVerticalScrollBarVisibility(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetExtentHeight() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetExtentHeight(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetExtentWidth() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetExtentWidth(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetHorizontalOffset() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetHorizontalOffset(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetScrollableHeight() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetScrollableHeight(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetScrollableWidth() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetScrollableWidth(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetVerticalOffset() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetVerticalOffset(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetViewportHeight() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetViewportHeight(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public float GetViewportWidth() {
    float ret = NoesisGUI_PINVOKE.ScrollViewer_GetViewportWidth(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public ScrollContentPresenter GetScrollInfo() {
    IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_GetScrollInfo(swigCPtr);
    ScrollContentPresenter ret = (cPtr == IntPtr.Zero) ? null : new ScrollContentPresenter(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetScrollInfo(ScrollContentPresenter scrollInfo) {
    NoesisGUI_PINVOKE.ScrollViewer_SetScrollInfo(swigCPtr, ScrollContentPresenter.getCPtr(scrollInfo));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void InvalidateScrollInfo() {
    NoesisGUI_PINVOKE.ScrollViewer_InvalidateScrollInfo(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void ScrollToHorizontalOffset(float offset) {
    NoesisGUI_PINVOKE.ScrollViewer_ScrollToHorizontalOffset(swigCPtr, offset);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void ScrollToVerticalOffset(float offset) {
    NoesisGUI_PINVOKE.ScrollViewer_ScrollToVerticalOffset(swigCPtr, offset);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty CanContentScrollProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_CanContentScrollProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ComputedHorizontalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ComputedHorizontalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ComputedVerticalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ComputedVerticalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ExtentHeightProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ExtentHeightProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ExtentWidthProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ExtentWidthProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HorizontalOffsetProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_HorizontalOffsetProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HorizontalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_HorizontalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ScrollableHeightProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ScrollableHeightProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ScrollableWidthProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ScrollableWidthProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty VerticalOffsetProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_VerticalOffsetProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty VerticalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_VerticalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ViewportHeightProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ViewportHeightProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ViewportWidthProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_ViewportWidthProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty PanningModeProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_PanningModeProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty PanningDecelerationProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_PanningDecelerationProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty PanningRatioProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ScrollViewer_PanningRatioProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr unityType = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_ScrollViewer(unityType);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_ScrollViewer(valPtr);
  }

}

}

