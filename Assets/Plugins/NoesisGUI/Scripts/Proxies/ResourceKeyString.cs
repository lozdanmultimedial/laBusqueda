/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class ResourceKeyString : BaseComponent {

  internal ResourceKeyString(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.ResourceKeyString_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(ResourceKeyString obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public ResourceKeyString() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(ResourceKeyString)) {
      return NoesisGUI_PINVOKE.new_ResourceKeyString();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public string Get() {
    string ret = NoesisGUI_PINVOKE.ResourceKeyString_Get(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public string GetStr() {
    IntPtr cPtr = NoesisGUI_PINVOKE.ResourceKeyString_GetStr(swigCPtr);
    NsString nsstring = new NsString(cPtr, false);
    string ret = nsstring.c_str();
    return ret;
}


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_ResourceKeyString(valPtr);
  }

}

}

