/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class HeaderedContentControl : ContentControl {

  internal HeaderedContentControl(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.HeaderedContentControl_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(HeaderedContentControl obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public HeaderedContentControl() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(HeaderedContentControl)) {
      return NoesisGUI_PINVOKE.new_HeaderedContentControl();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public bool GetHasHeader() {
    bool ret = NoesisGUI_PINVOKE.HeaderedContentControl_GetHasHeader(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public BaseComponent GetHeader() {
    IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_GetHeader(swigCPtr);
    BaseComponent ret = (cPtr == IntPtr.Zero) ? null : new BaseComponent(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetHeader(BaseComponent header) {
    NoesisGUI_PINVOKE.HeaderedContentControl_SetHeader__SWIG_0(swigCPtr, BaseComponent.getCPtr(header));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void SetHeader(string header) {
    NoesisGUI_PINVOKE.HeaderedContentControl_SetHeader__SWIG_1(swigCPtr, header);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public DataTemplate GetHeaderTemplate() {
    IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_GetHeaderTemplate(swigCPtr);
    DataTemplate ret = (cPtr == IntPtr.Zero) ? null : new DataTemplate(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetHeaderTemplate(DataTemplate dataTemplate) {
    NoesisGUI_PINVOKE.HeaderedContentControl_SetHeaderTemplate(swigCPtr, DataTemplate.getCPtr(dataTemplate));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public DataTemplateSelector GetHeaderTemplateSelector() {
    IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_GetHeaderTemplateSelector(swigCPtr);
    DataTemplateSelector ret = (cPtr == IntPtr.Zero) ? null : new DataTemplateSelector(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetHeaderTemplateSelector(DataTemplateSelector selector) {
    NoesisGUI_PINVOKE.HeaderedContentControl_SetHeaderTemplateSelector(swigCPtr, DataTemplateSelector.getCPtr(selector));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty HasHeaderProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_HasHeaderProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HeaderProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_HeaderProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HeaderTemplateProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_HeaderTemplateProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HeaderTemplateSelectorProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.HeaderedContentControl_HeaderTemplateSelectorProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr unityType = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_HeaderedContentControl(unityType);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_HeaderedContentControl(valPtr);
  }

}

}

