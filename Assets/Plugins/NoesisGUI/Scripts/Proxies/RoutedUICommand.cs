/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class RoutedUICommand : RoutedCommand {

  internal RoutedUICommand(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.RoutedUICommand_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(RoutedUICommand obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  protected RoutedUICommand() {
  }

  public string GetText() {
    string ret = NoesisGUI_PINVOKE.RoutedUICommand_GetText(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetText(string text) {
    NoesisGUI_PINVOKE.RoutedUICommand_SetText(swigCPtr, text);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_RoutedUICommand(valPtr);
  }

}

}

