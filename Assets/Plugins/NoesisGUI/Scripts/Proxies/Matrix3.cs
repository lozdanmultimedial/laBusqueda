/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class Matrix3 : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal Matrix3(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(Matrix3 obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~Matrix3() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          if (Noesis.Extend.Initialized) { NoesisGUI_PINVOKE.delete_Matrix3(swigCPtr);}
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public Vector3 this[uint i] {
    set {
      if (i >= 3) { throw new System.IndexOutOfRangeException(); }
      IndexSet(i, value);
    }
    get {
      if (i >= 3) { throw new System.IndexOutOfRangeException(); }
      return IndexGet(i);
    }
  }

  public static Matrix3 operator*(Matrix3 m, float f) {
    return new Matrix3(m[0] * f, m[1] * f, m[2] * f);
  }

  public static Matrix3 operator*(float f, Matrix3 m) {
    return m * f;
  }

  public static Matrix3 operator/(Matrix3 m, float f) {
    if (f == 0.0f) { throw new System.DivideByZeroException(); }
    return new Matrix3(m[0] / f, m[1] / f, m[2] / f);
  }

  public static Vector3 operator*(Vector3 v, Matrix3 m) {
    return new Vector3(
      v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0],
      v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1],
      v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2]);
  }

  public static Matrix3 operator*(Matrix3 m0, Matrix3 m1) {
    return new Matrix3(
      new Vector3(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0] + m0[0][2] * m1[2][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1] + m0[0][2] * m1[2][1],
        m0[0][0] * m1[0][2] + m0[0][1] * m1[1][2] + m0[0][2] * m1[2][2]),
      new Vector3(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0] + m0[1][2] * m1[2][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1] + m0[1][2] * m1[2][1],
        m0[1][0] * m1[0][2] + m0[1][1] * m1[1][2] + m0[1][2] * m1[2][2]),
      new Vector3(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m0[2][2] * m1[2][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m0[2][2] * m1[2][1],
        m0[2][0] * m1[0][2] + m0[2][1] * m1[1][2] + m0[2][2] * m1[2][2])
    );
  }

  public static Matrix3 operator*(Transform2 m0, Matrix3 m1) {
    return new Matrix3(
      new Vector3(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1],
        m0[0][0] * m1[0][2] + m0[0][1] * m1[1][2]),
      new Vector3(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1],
        m0[1][0] * m1[0][2] + m0[1][1] * m1[1][2]),
      new Vector3(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m1[2][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m1[2][1],
        m0[2][0] * m1[0][2] + m0[2][1] * m1[1][2] + m1[2][2])
    );
  }

  public static Matrix3 operator*(Matrix3 m0, Transform2 m1) {
    return new Matrix3(
      new Vector3(
        m0[0][0] * m1[0][0] + m0[0][1] * m1[1][0] + m0[0][2] * m1[2][0],
        m0[0][0] * m1[0][1] + m0[0][1] * m1[1][1] + m0[0][2] * m1[2][1],
        m0[0][2]),
      new Vector3(
        m0[1][0] * m1[0][0] + m0[1][1] * m1[1][0] + m0[1][2] * m1[2][0],
        m0[1][0] * m1[0][1] + m0[1][1] * m1[1][1] + m0[1][2] * m1[2][1],
        m0[1][2]),
      new Vector3(
        m0[2][0] * m1[0][0] + m0[2][1] * m1[1][0] + m0[2][2] * m1[2][0],
        m0[2][0] * m1[0][1] + m0[2][1] * m1[1][1] + m0[2][2] * m1[2][1],
        m0[2][2])
    );
  }

  public static bool operator==(Matrix3 m0, Matrix3 m1) {
    // If both are null, or both are same instance, return true
    if (System.Object.ReferenceEquals(m0, m1)) {
      return true;
    }

    // If one is null, but not both, return false
    if (((System.Object)m0 == null) || ((System.Object)m1 == null)) {
      return false;
    }

    // Return true if the fields match
    return m0[0] == m1[0] && m0[1] == m1[1] && m0[2] == m1[2];
  }

  public static bool operator!=(Matrix3 m0, Matrix3 m1) {
    return !(m0 == m1);
  }

  public override bool Equals(System.Object obj) {
    // If parameter is null return false
    if (obj == null) {
      return false;
    }

    // If parameter cannot be cast return false
    Matrix3 m = obj as Matrix3;
    if ((System.Object)m == null) {
      return false;
    }

    // Return true if the fields match
    return this[0] == m[0] && this[1] == m[1] && this[2] == m[2];
  }

  public bool Equals(Matrix3 m) {
    // If parameter is null return false
    if ((System.Object)m == null) {
        return false;
    }

    // Return true if the fields match
    return this[0] == m[0] && this[1] == m[1] && this[2] == m[2];
  }

  public override int GetHashCode() {
    return (this[0].GetHashCode() ^ this[1].GetHashCode()) ^ this[2].GetHashCode();
  }

  public Matrix3() : this(NoesisGUI_PINVOKE.new_Matrix3__SWIG_0(), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix3(Matrix3 m) : this(NoesisGUI_PINVOKE.new_Matrix3__SWIG_1(Matrix3.getCPtr(m)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix3(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22) : this(NoesisGUI_PINVOKE.new_Matrix3__SWIG_2(m00, m01, m02, m10, m11, m12, m20, m21, m22), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix3(Vector3 v0, Vector3 v1, Vector3 v2) : this(NoesisGUI_PINVOKE.new_Matrix3__SWIG_3(Vector3.getCPtr(v0), Vector3.getCPtr(v1), Vector3.getCPtr(v2)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Matrix3(Transform2 m) : this(NoesisGUI_PINVOKE.new_Matrix3__SWIG_4(Transform2.getCPtr(m)), true) {
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static Matrix3 Identity() {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Identity(), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 Scale(float scaleX, float scaleY, float scaleZ) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Scale(scaleX, scaleY, scaleZ), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 RotX(float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_RotX(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 RotY(float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_RotY(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 RotZ(float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_RotZ(radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 EulerZXY(float x, float y, float z) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_EulerZXY(x, y, z), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 Transpose(Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Transpose(Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static bool IsAffine(Matrix3 m) {
    bool ret = NoesisGUI_PINVOKE.Matrix3_IsAffine(Matrix3.getCPtr(m));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static float Determinant(Matrix3 m) {
    float ret = NoesisGUI_PINVOKE.Matrix3_Determinant(Matrix3.getCPtr(m));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 Inverse(Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Inverse__SWIG_0(Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 Inverse(Matrix3 m, float determinant) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Inverse__SWIG_1(Matrix3.getCPtr(m), determinant), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void Decompose(Matrix3 m, ref float scaleX, ref float scaleY, ref float scaleZ, ref float shearXY, ref float shearXZ, ref float shearZY, ref float rotateX, ref float rotateY, ref float rotateZ) {
    NoesisGUI_PINVOKE.Matrix3_Decompose(Matrix3.getCPtr(m), ref scaleX, ref scaleY, ref scaleZ, ref shearXY, ref shearXZ, ref shearZY, ref rotateX, ref rotateY, ref rotateZ);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static Matrix3 Orthonormalize(Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_Orthonormalize(Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PreScale(float scaleX, float scaleY, float scaleZ, Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PreScale(scaleX, scaleY, scaleZ, Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PreRotX(float radians, Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PreRotX(radians, Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PreRotY(float radians, Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PreRotY(radians, Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PreRotZ(float radians, Matrix3 m) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PreRotZ(radians, Matrix3.getCPtr(m)), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PostScale(Matrix3 m, float scaleX, float scaleY, float scaleZ) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PostScale(Matrix3.getCPtr(m), scaleX, scaleY, scaleZ), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PostRotX(Matrix3 m, float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PostRotX(Matrix3.getCPtr(m), radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PostRotY(Matrix3 m, float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PostRotY(Matrix3.getCPtr(m), radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static Matrix3 PostRotZ(Matrix3 m, float radians) {
    Matrix3 ret = new Matrix3(NoesisGUI_PINVOKE.Matrix3_PostRotZ(Matrix3.getCPtr(m), radians), true);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void ToEulerZXY(Matrix3 m, ref float x, ref float y, ref float z) {
    NoesisGUI_PINVOKE.Matrix3_ToEulerZXY(Matrix3.getCPtr(m), ref x, ref y, ref z);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  private Vector3 IndexGet(uint i) {
    Vector3 ret = new Vector3(NoesisGUI_PINVOKE.Matrix3_IndexGet(swigCPtr, i), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  private void IndexSet(uint i, Vector3 v) {
    NoesisGUI_PINVOKE.Matrix3_IndexSet(swigCPtr, i, Vector3.getCPtr(v));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

}

}

