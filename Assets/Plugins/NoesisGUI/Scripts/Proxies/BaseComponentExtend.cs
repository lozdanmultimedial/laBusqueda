using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Noesis
{

    public partial class BaseComponent
    {
        protected BaseComponent()
        {
            Type type = this.GetType();
            if (Noesis.Extend.NeedsCreateCPtr(type))
            {
                Init(CreateCPtr(type), true);
            }
            else
            {
                Init(Noesis.Extend.GetCPtr(this, type), false);
            }
        }

        private void Init(System.IntPtr cPtr, bool cMemoryOwn)
        {
            swigCPtr = new HandleRef(this, cPtr);

            if (cPtr != IntPtr.Zero && !cMemoryOwn)
            {
                AddReference();
            }

            #if UNITY_EDITOR
            if (NoesisGUI_PINVOKE.SWIGPendingException.Pending)
                throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
            #endif
        }

        protected virtual System.IntPtr CreateCPtr(System.Type type)
        {
            return Noesis.Extend.New(type, this);
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            Noesis.Extend.PropertyChanged(this.GetType(), swigCPtr.Handle, propertyName);
        }

        public string AsString()
        {
            return UnboxString();
        }

        public T As<T>() where T : BaseComponent
        {
            var obj = BaseComponent.AsType(typeof(T), swigCPtr.Handle, this);
            return obj == null ? null : (obj as T);
        }

        internal static object AsType(Type type, IntPtr cPtr, BaseComponent obj)
        {
            if (cPtr == IntPtr.Zero)
            {
                return null;
            }

            if (Noesis.Extend.IsExtendType(type))
            {
                object instance = Noesis.Extend.TryGetInstance(cPtr);
                return type.IsInstanceOfType(instance) ? instance : null;
            }
            else
            {
                BaseComponent component = obj != null ? obj : new BaseComponent(cPtr, false);
                MethodInfo checkType = type.GetMethod("CheckType", BindingFlags.NonPublic | BindingFlags.Static);
                if ((bool)checkType.Invoke(null, new object[] { component }))
                {
                    return Activator.CreateInstance(type, BindingFlags.CreateInstance |
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null,
                        new object[] { cPtr, false }, null);
                }

                return null;
            }
        }

        public static bool operator ==(BaseComponent a, BaseComponent b)
        {
            // If both are null, or both are the same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if ((object)a == null || (object)b == null)
            {
                return false;
            }

            // Return true if wrapped c++ objects match:
            return a.swigCPtr.Handle == b.swigCPtr.Handle;
        }

        public static bool operator !=(BaseComponent a, BaseComponent b)
        {
            return !(a == b);
        }

        public override bool Equals(object o)
        {
            return this == o as BaseComponent;
        }

        public override int GetHashCode()
        {
            return swigCPtr.Handle.GetHashCode();
        }
    }

}