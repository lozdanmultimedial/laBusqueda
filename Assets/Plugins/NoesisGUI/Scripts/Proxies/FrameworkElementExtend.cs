using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class FrameworkElement
    {
        public T FindName<T>(string name) where T : BaseComponent
        {
            BaseComponent element = FindName(name);
            return element == null ? null : element.As<T>();
        }

        public T FindResource<T>(object key) where T : BaseComponent
        {
            if (key is string)
            {
                return FindStringResource<T>(key as string);
            }

            if (key is System.Type)
            {
                Type type = key as Type;
                return FindTypeResource<T>(type.Name);
            }

            throw new Exception("Only String or Type resource keys supported.");
        }

        public T FindStringResource<T>(string resourceKeyString) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyString);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_FindStringResource(swigCPtr, stringPtr);
            #if UNITY_EDITOR
            if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
            #endif
            BaseComponent baseComponent = new BaseComponent(cPtr, false);
            return baseComponent.As<T>();
        }

        public T FindTypeResource<T>(string resourceKeyType) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyType);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_FindTypeResource(swigCPtr, stringPtr);
            #if UNITY_EDITOR
            if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
            #endif
            BaseComponent baseComponent = new BaseComponent(cPtr, false);
            return baseComponent.As<T>();
        }

        public T TryFindResource<T>(object key) where T : BaseComponent
        {
            if (key is string)
            {
                return TryFindStringResource<T>(key as string);
            }

            if (key is System.Type)
            {
                Type type = key as Type;
                return TryFindTypeResource<T>(type.Name);
            }

            throw new Exception("Only String or Type resource keys supported.");
        }

        public T TryFindStringResource<T>(string resourceKeyString) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyString);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_TryFindStringResource(swigCPtr, stringPtr);
            if (cPtr != IntPtr.Zero)
            {
                BaseComponent baseComponent = new BaseComponent(cPtr, false);
                return baseComponent.As<T>();
            }
            else
            {
                return null;
            }
        }

        public T TryFindTypeResource<T>(string resourceKeyType) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyType);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_TryFindTypeResource(swigCPtr, stringPtr);
            if (cPtr != IntPtr.Zero)
            {
                BaseComponent baseComponent = new BaseComponent(cPtr, false);
                return baseComponent.As<T>();
            }
            else
            {
                return null;
            }
        }
    }

}