﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

//como se hace el include del json ese? xD

public class Armar_Personaje : MonoBehaviour {
	List<GameObject> listaObjetos;

	//para guardar en el json
	List<v3> posiciones;
	List<miColor> colores;
	List<PrimitiveType> primitivas;
	//************************

	[SerializeField]
	Button cube;
	[SerializeField]
	Button sphere;
	[SerializeField]
	Button cylinder;
	[SerializeField]
	Button fin;

	Vector3 position;
	GameObject personaje;
	// Use this for initialization
	void Start () {
		personaje = new GameObject("Personaje");
		position = new Vector3 (0, 0, 0);

		//inicializacion listas..
		listaObjetos = new List<GameObject> ();
		posiciones = new List<v3> ();
		colores = new List<miColor> ();
		primitivas = new List<PrimitiveType> ();

		//Inicializacion botones (se le asigna el metodo)
		cube.onClick.AddListener (() => InstantiateCube());
		sphere.onClick.AddListener (() => InstantiateSphere());
		cylinder.onClick.AddListener (() => InstantiateCylinder());
		fin.onClick.AddListener (() => Fin());

	}
	void InstantiateSphere(){
		PrimitiveType primitiva = PrimitiveType.Sphere;
		primitivas.Add (primitiva);
		CreatePrimitive (primitiva);
		position.y += 1;
	}
	void InstantiateCube(){
		PrimitiveType primitiva = PrimitiveType.Cube;
		primitivas.Add (primitiva);
		CreatePrimitive (primitiva);
		position.y += 1;
	}
	void InstantiateCylinder(){
		PrimitiveType primitiva = PrimitiveType.Cylinder;
		primitivas.Add (primitiva);
		position.y += 1;
		CreatePrimitive (primitiva);
		position.y += 1;
	}
	void CreatePrimitive(PrimitiveType primitiva){
		GameObject go = GameObject.CreatePrimitive (primitiva);
		listaObjetos.Add (go);
		Renderer r = go.GetComponent<Renderer> (); //para setearle el material
		r.material = new Material (Shader.Find ("Diffuse"));
		r.material.color = Color.blue;
		go.transform.position = position;
		go.transform.parent = personaje.transform;
	}
	void Fin(){
		//string para guardar el resultado del jsonparser
		string s;
		//creo la estructura a parsear
		CharacterStruct c = new CharacterStruct ();
		//paso las litas a valores que pueda guardar en la estructura
		for (int i = 0; i < listaObjetos.Count; i++) {
			//posiciones
			Vector3 aux = listaObjetos[i].transform.position;
			v3 v = new v3(aux.x,aux.y,aux.z);
			posiciones.Add(v);
			//colores
			Color uncolorfeo = listaObjetos[i].GetComponent<Renderer>().material.color;
			miColor mcDonalds = new miColor(uncolorfeo.a,uncolorfeo.r,uncolorfeo.g,uncolorfeo.b);
			colores.Add(mcDonalds);
		}
		//las convierto en arrays
		v3[] pos = posiciones.ToArray ();
		miColor[] color = colores.ToArray();
		PrimitiveType[] pt = primitivas.ToArray();
		//los guardo en la estructura
		c.color = color;
		c.pos = pos;
		c.primitive = pt;
		c.numeroObjetos = listaObjetos.Count;
		//serializo a json la estructura
		s = JsonConvert.SerializeObject (c);
		//la loggueo xqsi para hacerme el pro
		Debug.Log (s);
		//la guardo en un lugar...
		Info.modeloDelPersonajeJson = s;
		print(s);
		//pasar a la siguiente escena...
		SceneManager.LoadScene ("Escena 1");
	}
}
