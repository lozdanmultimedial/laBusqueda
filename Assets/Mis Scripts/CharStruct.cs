using UnityEngine;

//estructura del objeto a parsear como "modelo del personaje"
public struct CharacterStruct{
	//o asi ok probamos
	public int numeroObjetos;
	public v3[] pos;
	public PrimitiveType[] primitive;
	public miColor[] color;
}

//vector3 nuestro ya que el vector3 de unity es raro y no lo convierte a json
//este vector3 va a servir tmb para sincronizar la posicion de los jugadores
//pero tmb vamos a necesitar uno para rotacion tmb
//mepa q tmb tenemos q pasar un json al servidor par las posiciones
public struct v3{
	public v3(float x, float y, float z){
		_x = x;
		_y = y;
		_z = z;
	}
	public float _x;
	public float _y;
	public float _z;
}
//lo mismo que antes pero para el color -.-
public struct miColor{
	public miColor(float a, float r, float g, float b){
		//lol si aguante remoto:v
		_a = a;
		_r = r;
		_g = g;
		_b = b;
	}
	public float _a;
	public float _r;
	public float _g;
	public float _b;
}