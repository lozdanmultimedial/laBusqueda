﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;

public class Cargar_Personaje : MonoBehaviour {
	void Start () {
		crearDesdeJSON ();
	}
	void crearDesdeJSON(){
		CharacterStruct personaje;

		personaje = JsonConvert.DeserializeObject<CharacterStruct> (Info.modeloDelPersonajeJson);

		for (int i = 0; i < personaje.numeroObjetos; i++) {
			Color color = new Color(personaje.color[i]._r,personaje.color[i]._g,personaje.color[i]._b,personaje.color[i]._a);
			Vector3 pos = new Vector3(personaje.pos[i]._x ,personaje.pos[i]._y,personaje.pos[i]._z);
			PrimitiveType primitiva = personaje.primitive[i];
			CreatePrimitive(color,primitiva,pos);
		}

	}

	void CreatePrimitive(Color color,PrimitiveType primitiva, Vector3 pos){//si pero la vamos a armar arriba creo
		GameObject go = GameObject.CreatePrimitive (primitiva);
		Renderer r = go.GetComponent<Renderer> ();
		r.material = new Material (Shader.Find ("Diffuse"));
		r.material.color = color;
		go.transform.position = pos;
		go.transform.parent = gameObject.transform;
	}
}
