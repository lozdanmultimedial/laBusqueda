﻿using UnityEngine;
using System.Collections;

public class Destello : MonoBehaviour 
{

	private float startPositionY ;
	private float startPositionX ;
	private float startPositionZ ;

	private float alto_pantalla = 130;
	private float bajo_pantalla = -130;
	private float lat_izq_pantalla = 500;
	private float lat_dch_pantalla = -360;
	float y;
	float x;
	float z;
    public GameObject luz;
	
	float k = 1;
	float []filtro = new float[2];
	
	//------------------------- Sonido
	
	
	public float vol;

	
	
	// -----------------------------------
	
	
	Vector3 startScale;
	
	void Start()
	{
		startScale = transform.localScale;
		startPositionY = transform.localPosition.y;
		startPositionX = transform.localPosition.x;
		startPositionZ = 1;
		filtro [0] = 0.0f;
		filtro [1] = 0.0f;
	}
	
	public void Update() 
	{
		// Metodo para pasar valor entre dos scripts de dos objetos diferentes.
		MoverPersonaje_Sonido volumen = GameObject.FindGameObjectWithTag("Player").GetComponent<MoverPersonaje_Sonido>();
		vol = volumen.loudness;
		
		print ("vol = " + vol);


		luz.GetComponent<Light>().intensity = vol * 100 ;
		filtro[0]=((0.01f * vol) + (0.99f * filtro[1]))/7.1f;
		float ang = Random.Range (-2.0f, 2f);
		float coseno = Mathf.Cos (ang);
		float seno = Mathf.Sin (ang); 
		x = startPositionX * filtro [0]* coseno * k;
		y = startPositionY * filtro [0]* seno * k ;
		z = startPositionZ * filtro [0]* k;
		print ("x = " + x + "  y = " + y + " z = " + z);
		if (x >= lat_izq_pantalla) { x = lat_izq_pantalla - x;}
		if (x <= lat_dch_pantalla) { x = lat_dch_pantalla - x;}
		if (y >= alto_pantalla) { y = alto_pantalla - y;}
		if (y <= bajo_pantalla) { y = bajo_pantalla + y;}

		transform.Translate (x, y, z * -1, Space.World);

		k = k * -1;
		filtro[1] = filtro[0];
	
		//transform.localScale = new Vector3(vol * startScale.x, vol * startScale.y, vol * startScale.z);
	}// end Update

}
