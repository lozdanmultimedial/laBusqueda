﻿using UnityEngine;
using System.Collections;

public class lamparita : MonoBehaviour 
{

	public GameObject luz;
	
	//------------------------- -------- Sonido
	float []filtro = new float[2];
	public float vol;
	
	// -----------------------------------

	
	void Start()
	{

		filtro [0] = 0.0f;
		filtro [1] = 0.0f;
	}
	
	public void Update() 
	{
		// Metodo para pasar valor entre dos scripts de dos objetos diferentes.
		MoverPersonaje_Sonido volumen = GameObject.FindGameObjectWithTag("Player").GetComponent<MoverPersonaje_Sonido>();
		vol = volumen.loudness;
		
		print ("vol = " + vol);
		
		luz.GetComponent<Light>().intensity = vol/3.1f ;
		filtro[0]=((0.01f * vol) + (0.99f * filtro[1]))/7.1f;

		filtro[1] = filtro[0];
		

	}// end Update
	
}