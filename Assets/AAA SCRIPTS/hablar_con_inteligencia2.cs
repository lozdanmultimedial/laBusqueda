﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class hablar_con_inteligencia2 : MonoBehaviour 
{

	string entrada ;
	bool choque;
	bool aprete_Return = false;
	bool sabio_Pregunta = true;
	public AudioClip[] cancion ;
	AudioSource audio;

	string usuario_dice;
	int sabio_dice;
	string [] partesEntrada;
	string [] partesRespuesta;
	int x = 1;// arioso de donde provienes
	int cont = 0;
	int cuadroEscenico = 1;
	//---------------------------------------------------------------------

	Charla [] hablando = new Charla[8];

	public class Charla : MonoBehaviour
	{
		public string tg;
		public int sabio_responde;
		
		public Charla (string jugador_dijo, int sabio_respondera) // constructor
		{
			tg = jugador_dijo;
			sabio_responde = sabio_respondera;
		}
	}// end class Charla
		
	void Start () 
	{
		audio = GetComponent<AudioSource>();
		entrada = "Mi nombre es: ";
		// el indice 0 es para disparar el aria "No entiendo lo que me dices"
		// los indices 1 a 4 pertenecen al cuadro escenico 1 y del 5 en adelante al cuadro escenico 2
		// ---------------------  Respuestas fijas --------------------
		hablando [0] = new Charla ("SinElementos", 0);
		hablando [1] = new Charla (" ", 1);
		hablando [2] = new Charla (" ", 2);
		hablando [3] = new Charla (" ", 3);
		hablando [4] = new Charla (" ", 4);
		// --------------------
		hablando[5] = new Charla ("Cual es su nombre", 5);
		hablando[6] = new Charla ("Donde estoy?", 6);
		hablando[7]= new Charla ("Cual es mi origen", 7);
		// por el momento hay 8 elementos
	}
	
	void OnTriggerEnter(Collider otro )
	{
		if (otro.tag == "Player") 
		{
			choque = true;
		}// end if otro.tag == "Player"
	}
	
	void OnTriggerExit(Collider otro)
	{
		if (otro.tag == "Player") {
			choque = false;
		}
	}
	
	
	void OnGUI() 
	{
		print ("Estoy en OnGUI  y choque es = " + choque);
		if (choque) 
		{
			Event e = Event.current;
			print ("E = "+Event.current.type+ "tecla = "+e.keyCode);
			print ("aprete_Return return = " + aprete_Return);
			
			if (e.keyCode == KeyCode.Return) 
			{
				aprete_Return = true;
			} 
			else if (aprete_Return == false) 
			{
				if(cuadroEscenico == 1)
				{
					print ("cuadroEscenico = "+cuadroEscenico);
					if(cont == 0)
				   {
						print ("1er ENTRADA = "+entrada);
					 entrada = GUI.TextField (new Rect (100, 250, 600, 20), entrada, 140);
					}				   
				   if(cont == 1)
				   {
					 entrada = GUI.TextField (new Rect (100, 250, 600, 20), entrada, 140);
				   }
				   if( cont == 2)
				   {
				   	 entrada = GUI.TextField (new Rect (100, 250, 600, 20), entrada, 140);
				   }
				}// fin cuadro escenico 1
			}// end else if aprete return == false
			
			if (aprete_Return == true) 
			{
				print("texto entrada = "+ entrada);
				dialoga(entrada);
				cont++;
				x++; // x = 2 = arioso "De donde provienes". x = 3 = arioso "Sabes de que sistema estelar". x = 4 = arioso "Y de que planeta provienes"
				if (x == 2){entrada = "Provengo de la galaxia "; }
				if (x == 3){entrada = "Del sistema estelar "; }
				if (x == 4){entrada = "Del planeta "; }
			    if (x > 4) {cuadroEscenico = 2;}  // disparar aria " No conozco esos lugares"}
				aprete_Return = false;
			}
			
		}// end if choque
		
	}// end OnGUI

	public void dialoga(string jugador_dixit)
	{
		bool encontre = false;
		int contE = 0;// indice para el array partesEntrada
		int contH = 5;// indice del array  hablando ----- indice 5 es el 1er registro de rtas libres.
		int contPH = 0;// indice para el array partesRespuesta
		
		if (cuadroEscenico == 2) 
		{
			partesEntrada = jugador_dixit.Split (' ');

			for (int i = 0; i < partesEntrada.Length; i++) {
				Debug.Log (partesEntrada [i]);
			}// solo para debug
		
			while (encontre == false && contH < hablando.Length) {
				partesRespuesta = hablando [contH].tg.Split (' ');


				if (partesEntrada [contE] == partesRespuesta [contPH]) {
					encontre = true;
				} else if (contE < partesEntrada.Length) {
					contE++;
					if (contE >= partesEntrada.Length && contPH <= partesRespuesta.Length) {// compare todas las palabras de la entrada con un registro de la BD
						contPH++;
						contE = 0;
					}

					if (contPH >= partesRespuesta.Length && contH <= hablando.Length) { 
						contH++; // avanzo una posicion registro en la BAse de Datos
						contPH = 0;
						contE = 0;
					}
				}
				if (!encontre && contH >= hablando.Length) {
					x = 0;
				} // no se encontro nada
			}  // ------------------------------- end While -------------

			if (encontre) {
				x = hablando [contH].sabio_responde;
			} else {
				x = 0;// disarar arioso "No comprendo lo que me dices"
			}

		}// end if cuadroEscenico == 2
		print ("X = " + x);
		dispararAudio (x);
			
	}// end dialoga

	void dispararAudio( int indice)
	{
		audio.clip = cancion[indice];
		audio.PlayOneShot (cancion[indice]);
		if(!audio.isPlaying) { aprete_Return = false; }
	}
}// end class hablar con intelgencia
