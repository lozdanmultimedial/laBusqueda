﻿using UnityEngine;
using System.Collections;

public class Interaccion_Sonora : MonoBehaviour {

	private float amp = 1;
	private float[] smooth = new float[2];
	public GameObject luz;
	public float velocidad = 5.0f;

	void Start () {
		// initalising the filter
		for (int i = 0; i < 2; i++) {
			smooth [i] = 0.0f;	
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		//intensity of light, controlled by the amplitude of the sound
		luz.GetComponent<Light>().intensity = amp;
		transform.Rotate(0, 0,velocidad * Time.deltaTime * amp);// cambie a rotacion en "z"
	}// end update


	void OnAudioFilterRead (float[] data, int channels)
	{		
		for (var i = 0; i < data.Length; i = i + channels) {
			// the absolute value of every sample
			float absInput = Mathf.Abs(data[i]);
			// smoothening filter doing its thing
			smooth[0] = ((0.01f * absInput) + (0.99f * smooth[1]));
			// exaggerating the amplitude
			amp = smooth[0]*107;
			// it is a recursive filter, so it is doing its recursive thing
			smooth[1] = smooth[0];
		}
	}

}
