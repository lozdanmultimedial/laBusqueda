﻿using UnityEngine;
using System.Collections;

public class Vumetro : MonoBehaviour 
{
	public int detail = 50; 
	private float startPosition = 1;
	private float amp;
	private float []smooth = new float[2];
	public float amplificacion;
	public GameObject luz;

	Vector3 startScale;
	
	void Start()
	{
		startScale = transform.localScale;
		startPosition = transform.localPosition.y;
		for (int i = 0; i < 2; i++) { smooth [i] = 0.0f; }
	}
	
	void Update() 
	{
		float[]data = new float[detail];
		AudioListener.GetOutputData(data, 0); 
		float packagedData  = 0.0f;
		
		for(int x = 0; x < data.Length; x++)
		{
			packagedData += (data[x]);   
		}
		
		amp = packagedData/data.Length;
		
		smooth[0] = ((0.01f * amp) + (0.99f * smooth[1]));
		// exaggerating the amplitude
		amp = smooth[0]* amplificacion/17;
		// it is a recursive filter, so it is doing its recursive thing
		smooth[1] = smooth[0];

		transform.Translate (startPosition * amp,startPosition * amp, startPosition * amp,   Space.World);
		luz.GetComponent<Light>().intensity = amp * 3313;
		//transform.localScale = new Vector3((packagedData * amp) + startScale.x, (packagedData * amp) + startScale.y, (packagedData * amp) + startScale.z);
	}// end Update
}
