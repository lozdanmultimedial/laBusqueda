﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Conversacion1B : MonoBehaviour 
{
	bool choque = false;
	public InputField entrada;

	
	void Start () 
	{
		entrada.text = "Mi nombre es: ";
		entrada.DeactivateInputField ();
	}
	
	public void Borrar() { entrada.text = " "; }

	void OnTriggerEnter( Collider otro)
	{
		if (otro.tag == "Player") {
			entrada.ActivateInputField ();
		}
	}

}
