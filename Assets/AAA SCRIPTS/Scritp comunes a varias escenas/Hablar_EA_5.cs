﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic; // permite usar listas y diccionarios
using System.Linq;
using UnityEngine.UI;
using System; // permite el IComparable

public class Hablar_EA_5 : MonoBehaviour
{
	bool choque;
	//bool sabio_Pregunta = true;
	bool estoy_cantando = false;
	public AudioClip[] cancion = new AudioClip [8];
	AudioSource audio;
	public InputField usuario_dice;
	int sabio_dice;
	int que_aria_soy;
	Charla[] hablando = new Charla[8];// base de datos
	float dur = 0;
	
	List <QueEncontre> qe;
	
	public class Charla : MonoBehaviour
	{
		public string tg;
		public int[] sabio_responde;
		
		public Charla (string jugador_dijo, int sr0,int sr1, int sr2, int sr3, int sr4) // constructor
		{
			tg = jugador_dijo;
			sabio_responde = new int [] {sr0,sr1,sr2,sr3,sr4};
		}
	}// end class Charla
	
	public class QueEncontre
	{
		public int registro;
		public int coincidencias;
		
		public QueEncontre (int pos, int c)// guardo el reg de la BD q tuvo alguna coincidencia con la 
			// entrada y cuantas coincidencias tienen.
		{
			registro = pos;
			coincidencias = c;
		}
	}// end class Que Encontre
	
	void Start ()
	{
		audio = GetComponent<AudioSource> ();
		CargarBaseDatos ();
		qe = new List <QueEncontre> ();
	}

	void Update()
	{
		if (estoy_cantando)
		{
			 
			if (dur > 0)
			{
				dur -= Time.deltaTime;


			}
			if (dur <= 0)
			{

				estoy_cantando = false;
			}
		}


	}//end update


	public void dialoga (InputField jugador_dixit)
	{
		//print ("dialogando. Ud dijo = " + jugador_dixit);
	    if (!estoy_cantando) 
		{
			string [] partesEntrada = jugador_dixit.text.Split (' ');// cargar el array partesEntrada
		
			//recorre db
			for (int iBD = 0; iBD < hablando.Length; iBD++) {
				//separa el texto el registro en palabras
				int cant_coincidencias = 0;
				bool encontre = false;
			
				string [] partesRegistro = hablando [iBD].tg.Split (' ');
				//recorre palabras del registro
			
				for (int iprg = 0; iprg < partesRegistro.Length; iprg++) {
					//recorre el array de palabras de entrada
					for (int i = 0; i < partesEntrada.Length; i++) {
						//se supone que esto anda *
						//Debug.Log("Entrada: " + partesEntrada[i].ToLower() + " == " + partesRegistro [iprg].ToLower() + " ? "+(partesEntrada[i].ToLower().Equals(partesRegistro [iprg].ToLower())));//no, sale cuando vos digas (para vos es error ponele)
						if ((partesEntrada [i].ToLower ().Equals (partesRegistro [iprg].ToLower ()))) {
							encontre = true;
							cant_coincidencias++;
						}
					}
				}
			
				if (encontre) {
					QueEncontre aux = new QueEncontre (iBD, cant_coincidencias);
					qe.Add (aux);
					//Debug.Log ("QueEncontre : " + aux.registro + " ; " + aux.coincidencias);
				}
			}
		
			encontrarLaMayorCoincidencia ();
		}// end if(!no_estoy_cantando)
	}// end dialoga
	
	void encontrarLaMayorCoincidencia ()
	{
		int mx = 0;
		int queregistrosoy = 0;
		int j = 0;
		
		foreach (var item in qe) {
			if(item.coincidencias > mx){
				mx = item.coincidencias;
				queregistrosoy = item.registro;
			}
		}
		qe.Clear ();
		int index = UnityEngine.Random.Range(0,5);
		que_aria_soy = hablando [queregistrosoy].sabio_responde [index];
		dispararAudio (que_aria_soy);
		
	} // end encontrarLaMayorCoincidencia
	
	void dispararAudio (int indice)
	{
		dur = cancion [indice].length;
		estoy_cantando = true;
		audio.clip = cancion [indice];
		audio.PlayOneShot (cancion [indice]);
	}
	
	void CargarBaseDatos ()
	{
		hablando [0] = new Charla ("SinElementos", 0,1,2,3,4);
		hablando [1] = new Charla ("Cual es su nombre", 1,0,2,3,4);
		hablando [2] = new Charla ("Donde estoy?", 2,0,1,3,4);
		hablando [3] = new Charla ("Hola", 3,0,1,2,4);

		hablando [4] = new Charla ("Quien es usted",4,3,1,2,0 );
		//--------------
		hablando [5] = new Charla ("Sabe Ud cual es mi planeta de origen", 5,6,7,0,1);
		hablando [6] = new Charla ("Conoce Ud cual es mi planeta de origen", 5,6,7,0,1);
		hablando [7] = new Charla ("Cual es mi planeta de origen", 5,6,7,0,1);// ver hablando[1]

		/*
		//-------------
		hablando [8] = new Charla ("Como voy a mi planeta de origen", 6);
		hablando [9] = new Charla ("Donde esta mi planeta de origen", 7);
		hablando [10] = new Charla ("tendre exito en mi busqueda", 8);
		hablando [11] = new Charla ("Cual es mi destino", 9);
		*/
	}
}// END CLASS HABLAR CON INTELIGENCIA 4
