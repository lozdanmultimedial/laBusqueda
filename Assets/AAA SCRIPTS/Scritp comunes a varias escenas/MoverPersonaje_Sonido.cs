﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoverPersonaje_Sonido : MonoBehaviour 
{
	 public float speed ;
	 public float dir ;

	//------------------------- Sonido

	public float loudness = 0.0f;
	float sensitivity = 100.0f;
	float frequency = 0.0f;
	int samplerate = 11024;
	private AudioSource audioSource;
	string AudioInputDevice;
	public float umbral = 20.0f;

	public float divisor;
	public float multiplicador;

	// -----------------------------------

	void Start () 
	{
	    
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = Microphone.Start(null, true, 10, samplerate);
		audioSource.loop = true; // Set the AudioClip to loop
		//audioSource.mute = true; // Mute the sound, we don't want the player to hear it //-------> por ahi si queremos que los jugadores escuchen//
		while (!(Microphone.GetPosition(AudioInputDevice) > 0)){} // Wait until the recording has started
		audioSource.Play(); // Play the audio source!
	}
	

	void Update () 
	{
		
		analizarSonido ();
		if (loudness > umbral)
		{
		
			float vol = loudness * multiplicador;
			if (Input.GetKey (KeyCode.UpArrow))
				this.transform.Translate (0f, 0f, -speed * Time.deltaTime * dir * vol/divisor, Space.World);
		
			if (Input.GetKey (KeyCode.DownArrow))
				this.transform.Translate (0f, 0f, speed * Time.deltaTime * dir* vol/divisor, Space.World);
		
			if (Input.GetKey (KeyCode.RightArrow))
				this.transform.Translate (speed * Time.deltaTime * vol/divisor , 0f, 0f, Space.World);
		
			if (Input.GetKey (KeyCode.LeftArrow))
				this.transform.Translate (-speed * Time.deltaTime * vol/divisor, 0f, 0f, Space.World);
		}
	
	}//end update

	void analizarSonido()
	{


		loudness = GetAveragedVolume() * sensitivity;
		Debug.Log ("GetAveragedVolume() = "+GetAveragedVolume() +"  sensitivity = "+ sensitivity);

		frequency = GetFundamentalFrequency();
		Debug.Log ("vol = "+loudness +"  hz = "+ frequency);
	}

	float GetAveragedVolume()
	{
		float[] data = new float[256];
		float a = 0;
		audioSource.GetOutputData(data,0);

		foreach(float s in data)
		{
			a += Mathf.Abs(s);
		}
		Debug.Log (a);
		return a/256;
	}
	
	float GetFundamentalFrequency()
	{
		float fundamentalFrequency = 0.0f;
		float[] data = new float[8192];
		audioSource.GetSpectrumData(data,0,FFTWindow.BlackmanHarris);
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < data.Length; j++)
		{
			if ( s < data[j] )
			{
				s = data[j];
				i = j;
			}
		}
		fundamentalFrequency = i * samplerate / 8192;
		return fundamentalFrequency;
	}

}// end class MoverPersonaje_SONIDO


