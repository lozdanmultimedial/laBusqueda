﻿
// la funcion de filtro fue extraida de : http://re-sounding.com/2013/12/14/unity-controlling-game-elements-with-sound/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MoverPersoanje : MonoBehaviour {

	public float speed = 1;
	public float dir = 1;
	//--------------------------- sonido
	float amp;
	
	private float[] smooth = new float[2];
	public float amplificacion = 7.0f;
	public float umbralSonido;
	public int detail = 50; 
	//------------------------------------
	
	// Use this for initialization
	void Start () 
	{
		// initalising the filter
		for (int i = 0; i < 2; i++) { smooth [i] = 0.0f;	}

	}
	
	// Update is called once per frame
	void Update () 
	{
		analizarSonido ();

		if (Input.GetKey (KeyCode.UpArrow))
			this.transform.Translate (0f, 0f, speed * Time.deltaTime * dir  , Space.World);
		
		if (Input.GetKey (KeyCode.DownArrow))
			this.transform.Translate (0f, 0f, -speed * Time.deltaTime * dir , Space.World);
		
		if (Input.GetKey (KeyCode.RightArrow))
			this.transform.Translate ( speed *  Time.deltaTime ,0f, 0f, Space.World);
		
		if (Input.GetKey (KeyCode.LeftArrow))
			this.transform.Translate ( -speed * Time.deltaTime  ,0f, 0f, Space.World);
		
	}
	

	void analizarSonido()
	{
		float[]data = new float[detail];
		AudioListener.GetOutputData(data, 0); 
		float packagedData  = 0.0f;
		
		for(int k = 0; k < data.Length; k++)
		{
			packagedData += (data[k]);   
		}
		
		amp = packagedData/data.Length;

		print ("amp1 = " + amp);

		smooth[0] = ((0.01f * amp) + (0.99f * smooth[1]));
		// exaggerating the amplitude
		amp = smooth[0]* amplificacion;

		print ("amp2 = " + amp);

		// it is a recursive filter, so it is doing its recursive thing
		smooth[1] = smooth[0];
	}
	/*
	 * void OnAudioFilterRead (float[] data, int channels)
	{		
		float aux = 0;
		for (var i = 0; i < data.Length; i = i + channels) 
		{
			// the absolute value of every sample
			//float absInput = Mathf.Abs(data[i]);
			aux += data [i];
		}

		amp = aux / data.Length;
			// smoothening filter doing its thing
			//smooth[0] = ((0.01f * absInput) + (0.99f * smooth[1]));
			smooth[0] = ((0.01f * amp) + (0.99f * smooth[1]));
			// exaggerating the amplitude
			amp = smooth[0]*7;
			// it is a recursive filter, so it is doing its recursive thing
			smooth[1] = smooth[0];

	}
*/


	


}// end class
