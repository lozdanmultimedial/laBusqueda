﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class hablar_con_inteligencia : MonoBehaviour 
{

	string entrada ;
	bool choque;
	bool aprete_Return = false;
	bool sabio_Pregunta = true;
	public AudioClip[] cancion ;
	AudioSource audio;

	string usuario_dice;
	int sabio_dice;
	string [] partesEntrada;
	string [] partesRespuesta;
	int x;
	
	Charla [] hablando = new Charla[4];

	public class Charla : MonoBehaviour
	{
		public string tg;
		public int sabio_responde;
		
		public Charla (string jugador_dijo, int sabio_respondera) // constructor
		{
			tg = jugador_dijo;
			sabio_responde = sabio_respondera;
		}
	}// end class Charla
		
	void Start () {
		audio = GetComponent<AudioSource>();
		entrada = "Borre este texto y dialogue con el sabio en no mas de 140 caracteres, apritet la tecla enter al finaizar. ";

		hablando[0] = new Charla ("SinElementos", 0);
		hablando[1] = new Charla ("Cual es su nombre", 1);
		hablando[2] = new Charla ("Donde estoy?", 2);
		hablando[3] = new Charla ("Cual es mi origen", 3);
	}
	
	void OnTriggerEnter(Collider otro )
	{
		if (otro.tag == "Player") 
		{
			choque = true;
			print ("choque = "+choque);
		}// end if otro.tag == "Player"
	}
	
	void OnTriggerExit(Collider otro)
	{
		if (otro.tag == "Player") {
			choque = false;
		}
	}
	
	
	void OnGUI() 
	{
		print ("OnGUI");
		
		if (choque) 
		{
			Event e = Event.current;
			
			if (e.keyCode == KeyCode.Return) 
			{
				aprete_Return = true;
			} 
			else if (aprete_Return == false) 
			{
				entrada = GUI.TextField (new Rect (100, 250, 600, 20), entrada, 140);
			}
			
			if (aprete_Return == true) 
			{
				dialoga(entrada);
			}
			
		}// end if choque
		
	}// end OnGUI
	public void dialoga(string jugador_dixit)
	{
		bool encontre = false;
		int contE = 0;// indice para el array partesEntrada
		int contH = 0;// indice del array  hablando
		int contPH = 0;// indice para el array partesRespuesta
		
		partesEntrada = jugador_dixit.Split (' ');

		for( int i = 0; i < partesEntrada.Length; i++)////
		{
			Debug.Log (partesEntrada[i]);
		}
		
		while (encontre == false && contH < hablando.Length) 
		{
			partesRespuesta = hablando[contH].tg.Split (' ');


			if( partesEntrada[contE]== partesRespuesta[contPH] )
			{
				encontre = true;
			}
			else if(contE < partesEntrada.Length)
			{
				contE++;
				if(contE >= partesEntrada.Length && contPH <= partesRespuesta.Length)// compare todas las palabras de la entrada con un registro de la BD
				{
					contPH++;
					contE = 0;
				}


				if( contPH >= partesRespuesta.Length && contH <= hablando.Length)
				{ 
					contH++; // avanzo una posicion registro en la BAse de Datos
					contPH = 0;
					contE = 0;
				}
			}
			if( !encontre && contH >= hablando.Length){ x = 0; } // no se encontro nada
		}  // ------------------------------- end While -------------

		if (encontre) {
			x = hablando [contH].sabio_responde;
		} 
		else {
			x = 0;
		}

		dispararAudio (x);

		
	}// end dialoga

	void dispararAudio( int indice)
	{
		audio.clip = cancion[indice];
		audio.PlayOneShot (cancion[indice]);
		if(!audio.isPlaying) { aprete_Return = false; }
	}
}// end class hablar con intelgencia
