﻿#pragma strict

var cancion: AudioClip;
var cante = false;

function OnTriggerEnter( otro:  Collider)
{
   if(otro.tag == "Player") { DispararAudio(); }
}
   
   function DispararAudio()
   {
       if(!cante)
       {
          GetComponent.<AudioSource>().PlayOneShot(cancion);
          cante = true;
       }
   }
