﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Conversacion1 : MonoBehaviour 
{

	bool choque = false;
	public InputField entrada;
	private int cont = 0;
	private int indice = 0;
	public AudioClip[] cancion ;
	public AudioClip ariaFinal;

	AudioSource audio;

	public class SabioPregunta : MonoBehaviour
	{
		public int sabio_responde; 

		public SabioPregunta(int sabio_respondera)
		{
			sabio_responde = sabio_respondera;

		}
	}// end class SabioPregunta

	 public SabioPregunta [] pregunta = new SabioPregunta[4];

	void Start () /////////// START //////////////////////////////////////////
	{
		audio = GetComponent<AudioSource>();
		cancion = new AudioClip[6];
		entrada.text = "Mi nombre es: ";
		
		for (int i = 0; i < 4; i++) 
		{

			pregunta [i] = new SabioPregunta (i);
		}
		entrada.ActivateInputField ();
	}

	void Update()
	{
		if (!audio.isPlaying && choque) {
			DispararAudio ();
		}
	}
	public void Borrar() {
		entrada.text = " ";
		cont++;
	}
	
	void OnTriggerEnter( Collider otro)
	{
		if (otro.tag == "Player") {
			choque = true;
		}
	}
	public void Salir() { entrada.DeactivateInputField ();}

	public void DispararAudio()
	{
		print ("audio indice = " + indice);
		if (cont < 4) 
		{
			int j = indice;
			audio.clip = cancion [j];
			audio.PlayOneShot (cancion [j]);
			if (!audio.isPlaying && j < 4) { 
				indice++;
				print ("termino el aria "+j+" la proxima es = "+indice);
			}
		}
		else if ( cont >= 4)
		{
			audio.clip = ariaFinal;
			audio.PlayOneShot (ariaFinal);
		}

	}

	public void CambiarEscena()
	{
		SceneManager.LoadScene ("Escena 2");
	}

}// end class Conversacion1
