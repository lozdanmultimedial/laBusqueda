﻿#pragma strict

var planetario : GameObject;
var perimetro : GameObject;
var tiempoMax : float;
var conteoRegresivo : float = 19;
var randomX : float = 337;
var randomZ : float = 1073;
var randomY : float = 0;
var activo : boolean = false;
var instanciar : boolean = true;

function Start () 
{
    
}

function Update ()
{
   print("activo = "+activo+"  instanciar = "+instanciar);
   
   if(activo)
   {
	   conteoRegresivo -= Time.deltaTime;
	   if(conteoRegresivo <= 0)
	   {
	      transform.position.x += Random.Range(- randomX, randomX);
	      transform.position.y += Random.Range(- randomY, randomY);
	      transform.position.z += Random.Range(703, randomZ);
	      if( instanciar)
	      {
	         Instantiate(planetario, transform.position, transform.rotation);
	         instanciar = false;
	      }
	      conteoRegresivo += Random.Range(239,373);   
	   }
   }
   
}

function OnTriggerEnter( otro : Collider )
{
   if( otro.tag == "Player" && this.tag == "planetario" )
   {
      activo = true;
      print("choque activo = "+ activo);
   }
}

function OnTriggerExit( otro : Collider )
{
   if( otro.tag == "Player" && this.tag == "perimetro")
   instanciar = true;
   print("sali instanciar es = "+instanciar);
}