﻿using UnityEngine;
using System.Collections;

public class Translacion_orbital : MonoBehaviour 
{
	public float vel;
	//public float centro_x;

	public GameObject centro;  
	public float rotacionEjeX;
	public float rotacionEjeY;
	public float rotacionEjeZ;
	Transform c;
	float cx;
	float cy;
	float cz;

	void Start()
	{
		
	}

	void Update () 
	{

		c = centro.transform;
		cx = c.transform.position.x;
		cy = c.transform.position.y;
		cz = c.transform.position.z;
		transform.RotateAround (new Vector3 (cx, cy, cz), new Vector3 (rotacionEjeX, rotacionEjeY, rotacionEjeZ), vel);
		//                       centro                  eje de rotacion       incremento en el ángulo de giro
		//transform.RotateAround (new Vector3 (cx, cy, cz), new Vector3 (rotacionEjeX, rotacionEjeY, rotacionEjeZ), 1);
	}
}
