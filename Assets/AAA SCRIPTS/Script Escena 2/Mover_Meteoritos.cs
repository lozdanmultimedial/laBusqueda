﻿using UnityEngine;
using System.Collections;

public class Mover_Meteoritos : MonoBehaviour {
	private float zMov;
	private float timeDestroy = 0.0f;


	// Use this for initialization
	void Start () {
		zMov = Random.Range (-5, -20) * Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameObject.transform.Translate (0, 0, zMov);
		timeDestroy += Time.deltaTime;
		if (timeDestroy > 5)
			Destroy (this.gameObject);
	}
}
