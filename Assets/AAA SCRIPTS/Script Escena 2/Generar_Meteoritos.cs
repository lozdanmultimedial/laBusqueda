﻿using UnityEngine;
using System.Collections;

public class Generar_Meteoritos : MonoBehaviour {
	// este script no se esta usando
	public GameObject estrellas1;
	public GameObject estrellas2;
	public GameObject estrellas3;
	public GameObject estrellas4;
	private Vector3 randomVector1;
	private Vector3 randomVector4;
	private Vector3 randomVector2;
	private Vector3 randomVector3;
	
	void Update () 
	{
		randomVector1 = new Vector3 (Random.Range (-130, 130), Random.Range (-115, 113), 35);
		randomVector2 = new Vector3 (Random.Range (-130, 130), Random.Range (-115, 113), 35);
		randomVector3 = new Vector3 (Random.Range (-130, 130), Random.Range (-115, 113), 35);
		randomVector4 = new Vector3 (Random.Range (-130, 130), Random.Range (-115, 113), 35);
		Instantiate (estrellas1, randomVector1, transform.rotation);
		Instantiate (estrellas4, randomVector4, transform.rotation);
		Instantiate (estrellas2, randomVector2, transform.rotation);
		Instantiate (estrellas3, randomVector3, transform.rotation);
	}
}
