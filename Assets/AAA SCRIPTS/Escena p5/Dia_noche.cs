﻿using UnityEngine;
using System.Collections;

public class Dia_noche : MonoBehaviour {

	Vector3 pos;
	float azimut= 1;
	float latitud = 1;
	public float radio = 100;
	public GameObject sol;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		pos.x = radio * Mathf.Sin (azimut) * Mathf.Cos (latitud);
		pos.y = radio * Mathf.Sin (azimut) * Mathf.Sin (latitud);
		pos.z = radio * Mathf.Cos (azimut)/97;
		azimut *= Time.deltaTime/33;
		latitud *= Time.deltaTime/10;

		sol.transform.Translate (pos.x, pos.y, pos.z);
	}
}
