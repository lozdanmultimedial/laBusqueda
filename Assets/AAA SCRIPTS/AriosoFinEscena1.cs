﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AriosoFinEscena1 : MonoBehaviour {
	public AudioClip aria ;
	AudioSource audio;
	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource>();
	}
	
	public void DispararAudio()
	{

		audio.clip = aria;
		audio.PlayOneShot (aria);
		if (!audio.isPlaying) { SceneManager.LoadScene("escena 2");

		}
	}
}
