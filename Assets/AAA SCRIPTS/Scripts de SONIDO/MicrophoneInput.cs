﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneInput : MonoBehaviour {
	public float sensitivity = 100.0f;
	public float loudness = 0.0f;
	public float frequency = 0.0f;
	public int samplerate = 11024;
	private AudioSource audioSource;
	public string AudioInputDevice = "Realtek High Definition Audio";
	
	void Start() {
		audioSource = GetComponent<AudioSource> ();
		audioSource.clip = Microphone.Start(null, true, 10, samplerate);
		audioSource.loop = true; // Set the AudioClip to loop
		audioSource.mute = true; // Mute the sound, we don't want the player to hear it
		while (!(Microphone.GetPosition(AudioInputDevice) > 0)){} // Wait until the recording has started
		audioSource.Play(); // Play the audio source!
	}
	
	void Update(){
		loudness = GetAveragedVolume() * sensitivity;
		frequency = GetFundamentalFrequency();
	}
	
	float GetAveragedVolume()
	{
		float[] data = new float[256];
		float a = 0;
		audioSource.GetOutputData(data,0);
		foreach(float s in data)
		{
			a += Mathf.Abs(s);
		}
		return a/256;
	}
	
	float GetFundamentalFrequency()
	{
		float fundamentalFrequency = 0.0f;
		float[] data = new float[8192];
		audioSource.GetSpectrumData(data,0,FFTWindow.BlackmanHarris);
		float s = 0.0f;
		int i = 0;
		for (int j = 1; j < data.Length; j++)
		{
			if ( s < data[j] )
			{
				s = data[j];
				i = j;
			}
		}
		fundamentalFrequency = i * samplerate / 8192;
		return fundamentalFrequency;
	}
}
