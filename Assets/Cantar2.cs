﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cantar2 : MonoBehaviour 
{
	public AudioClip []aria;
	private AudioSource  _source;
	private int cont = -1;
	int cant_arias = 6;
	int cant_rtas = 4;
	//bool termino = false;
	bool medi_la_duracion_del_aria = false;
	float dur = 0;
	bool cantar = false;
	bool cambiarEscena = false;
	bool cantando = false;

	//--------------------------------- UI ----
	bool choque = false;
	public InputField entrada;
	public InputField usuario_responde;
	int j = 0; // indice para el array de respuestas del jugador

	public class JugadorResponde : MonoBehaviour
	{
		public string jugador_responde; 
		
		public JugadorResponde(string j_respondera)
		{
			jugador_responde = j_respondera;
		}
	}// end class SabioPregunta
	
	JugadorResponde [] respuesta = new JugadorResponde [4];

	
	void Start() 
	{ 
		_source = GetComponent<AudioSource>();

		respuesta[0] = new JugadorResponde ("Mi nombre es ...");
		respuesta[1] = new JugadorResponde ("Provengo del planeta ... ");
		respuesta[2] = new JugadorResponde ("Mi planeta se encuentra en el sistema estelar ...");
		respuesta[3] = new JugadorResponde ("El sistema estelar pertenece a la galaxia ...");
		//usuario_responde.text = respuesta [0].jugador_responde;

	}

	void Update()
	{

		/*print (" ");
		print ("COMIENZA el Loop ======================== ");
		print (" ");
		print ("condiciones de inicio: cantar = " + cantar + " cont = " + cont);
        */
		if (!cambiarEscena) 
		{
			if (cantar && cont >= 0 && cont < cant_arias) //cant_arias = 5 
			{ // 4 es el numero de arias a ejecutar

				print ("cont = "+cont);
				if (!medi_la_duracion_del_aria) 
				{ // no hay audio disparado
					dur = aria [cont].length;// tomo la duracion del aria actual
					//print ("el aria dura = " + dur);


					DispararAudio (cont);
					medi_la_duracion_del_aria = true;

				}    
				if (medi_la_duracion_del_aria) 
				{ 
					if (dur > 0) 
					{ // si no termino le resto el tiempo transcurrido
						dur -= Time.deltaTime;
						//print (" aun no termino el aria restan = " + dur);
						entrada.DeactivateInputField ();
					}
					if (dur <= 0) // termino de cantar el aria
					{ 
						cont++;// indice del aria siguente
						cantar = false;
						medi_la_duracion_del_aria = false; 
						entrada.ActivateInputField ();
						//print (" el aria termino. cont = " + cont+ "cantando = "+cantando);
						if ( cont >= 0 && cont < cant_rtas && cantando == true) 
						{

							AsignarIncipitRespuesta (cont);
						}
						cantando = false;

					}
				}// end else 
			}// end if  cantar and cont < cant_arias 
			if (cont == cant_arias - 1&& !cambiarEscena) 
			{ 
				entrada.text = "Escuche la respuesta y avance hacia el portal";
				cambiarEscena = true;
				DispararAudio (cant_arias -1);
				dur = aria [cont].length;

			}// se ejecuta el aria final Abrire el portal del universo

		}// end if(!cambiarEscena)
		if (cambiarEscena) 
		{
			dur -= Time.deltaTime;
			if(dur <= 0) { Application.LoadLevel ("Escena 2");}
	     }
		//print ("FIN del LOOP =============");
	
	}// end UPDATE

	void OnTriggerEnter(Collider otro)
	{
		print ("CHoque");
		if(otro.tag == "Player") 
		{
			cont = 0; 
			cantar = true;
		}
	}

	public void AsignarIncipitRespuesta( int j)
	{
		usuario_responde.text = respuesta [j].jugador_responde;
		print ("entrada = "+usuario_responde.text);
	}
	
	public void disparar()// este metodo se activa solo si se hace clikc en el boton "enviar"
	{
			cantar = true;
		//print ("toque el boton.  cante = "+cantar);
	}
		
	void DispararAudio(int indice)
	{
		GetComponent<AudioSource>().PlayOneShot (aria [indice]);
		cantando = true;
	}// end DisparaAudio
}
